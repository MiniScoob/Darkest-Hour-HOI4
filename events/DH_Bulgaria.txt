﻿###########################
# Darkest Hour  Events : Bulgaria
###########################

add_namespace = DH_Bulgaria

#########################################################################
# Bulgaria wants to buy Light Tanks from Italy
#########################################################################
country_event = {
	id = DH_Bulgaria.1
	title = DH_Bulgaria.1.t
	desc = DH_Bulgaria.1.d
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes
	# Sure	
	option = {
		name = DH_Bulgaria.1.A
		ai_chance = {
			factor = 70
		}
		send_equipment = {
			target = BUL
			type = armor_equipment
			amount = 50
		}
		add_timed_idea = { idea = BUL_Payment days = 180 }
		BUL = {
			country_event = DH_Bulgaria.2
		}
	}	
	# No
	option = { 
		name = DH_Bulgaria.1.B
		ai_chance = {
			factor = 30
		}	
	}	
}
#########################################################################
# Deal Concluded
#########################################################################
country_event = {
	id = DH_Bulgaria.2
	title = DH_Bulgaria.2.t
	desc = DH_Bulgaria.2.desc   
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes
	
	option = { # cool
		name = DH_Bulgaria.2.a
		ai_chance = {
			factor = 30
		}
		add_opinion_modifier = { target = FROM modifier = BUL_sold_planes}	
	}	
}

#fighter competition - initial choice for Bulgaria
country_event = {
	id = DH_Bulgaria.3
	title = DOD_hungary.150.t
	desc = DOD_hungary.150.desc 
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes

	option = {# light fighter
		name = DOD_hungary.150.a
		trigger = {
			OR = {
				GER = {
					is_available_fighter_BUL = yes
				}
				FRA = {
					is_available_fighter_BUL = yes
				}
				ITA = {
					is_available_fighter_BUL = yes
				}
				ENG = {
					is_available_fighter_BUL = yes
				}
				CZE = {
					is_available_fighter_BUL = yes
				}
			}
		}
		ai_chance = {
			factor = 50
			modifier = {
				has_tech = Fighter_1936
				add = -10
			}
			modifier = {
				has_tech = Fighter_1940
				add = -10
			}
			modifier = {
				has_tech = Fighter_1943
				factor = 0
			}
		}
		country_event = DH_Bulgaria.4
		
	}

	option = {# heavy fighter
		name = DOD_hungary.150.b
		trigger = {
			OR = {
				GER = {
					is_available_heavy_fighter_BUL = yes
				}
				FRA = {
					is_available_heavy_fighter_BUL = yes
				}
				ITA = {
					is_available_heavy_fighter_BUL = yes
				}
				ENG = {
					is_available_heavy_fighter_BUL = yes
				}
				CZE = {
					is_available_heavy_fighter_BUL = yes
				}
			}
		}
		ai_chance = {
			factor = 50
			modifier = {
				has_tech = heavy_Fighter_1936
				add = -10
			}
			modifier = {
				has_tech = heavy_Fighter_1940
				add = -10
			}
			modifier = {
				has_tech = heavy_Fighter_1943
				factor = 0
			}
		}
		country_event = DH_Bulgaria.5
	}
}

#Fighter competition - Bulgaria is looking for a light fighter
country_event = {
	id = DH_Bulgaria.4
	title = DOD_hungary.151.t
	desc = {
		text = DOD_hungary.151.desc_light_fighter
		trigger = {
			NOT = { 
				has_country_flag = GER_fighter_refused
				has_country_flag = ENG_fighter_refused
				has_country_flag = CZE_fighter_refused
				has_country_flag = FRA_fighter_refused
				has_country_flag = ITA_fighter_refused
			} 
		}
	}
	desc = {
		text = DOD_hungary.151.desc_light_fighter_refused
		trigger = {
			OR = {
		 		has_country_flag = GER_fighter_refused
				has_country_flag = ENG_fighter_refused
				has_country_flag = CZE_fighter_refused
				has_country_flag = FRA_fighter_refused
				has_country_flag = ITA_fighter_refused
		 	}
		}
	}
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes

	option = {# Select German Plane
		name = DOD_hungary.151.a
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = GER
				factor = 10
			}
			modifier = {
				GER = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				GER = {
					has_tech = Fighter_1936
				}
				add = 10
			}
			modifier = {
				GER = {
					has_tech = Fighter_1940
				}
				add = 10
			}
			modifier = {
				GER = {
					has_tech = Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					GER = { has_tech = Fighter_1936}
					BUL = { NOT = { has_tech = Fighter_1936 } }
				}
				AND = {
					GER = { has_tech = Fighter_1940}
					BUL = { NOT = { has_tech = Fighter_1940 } }
				}
				AND = {
					GER = { has_tech = Fighter_1943}
					BUL = { NOT = { has_tech = Fighter_1943 } }
				}
			}
			NOT = { has_war_with = GER }
			NOT = { has_country_flag = GER_fighter_refused }
		}
		GER = { country_event = DH_Bulgaria.6 }	
	}

	option = {# select italian plane
		name = DOD_hungary.151.b
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = ITA
				factor = 10
			}
			modifier = {
				ITA = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				ITA = {
					has_tech = Fighter_1936
				}
				add = 10
			}
			modifier = {
				ITA = {
					has_tech = Fighter_1940
				}
				add = 10
			}
			modifier = {
				ITA = {
					has_tech = Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					ITA = { has_tech = Fighter_1936}
					BUL = { NOT = { has_tech = Fighter_1936 } }
				}
				AND = {
					ITA = { has_tech = Fighter_1940}
					BUL = { NOT = { has_tech = Fighter_1940 } }
				}
				AND = {
					ITA = { has_tech = Fighter_1943}
					BUL = { NOT = { has_tech = Fighter_1943 } }
				}
			}
			NOT = { has_war_with = ITA }
			NOT = { has_country_flag = ITA_fighter_refused }
		}
		ITA = { country_event = DH_Bulgaria.6 }	
	}

	option = {# select french plane
		name = DOD_hungary.151.c
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = FRA
				factor = 10
			}
			modifier = {
				FRA = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				FRA = {
					has_tech = Fighter_1936
				}
				add = 10
			}
			modifier = {
				FRA = {
					has_tech = Fighter_1940
				}
				add = 10
			}
			modifier = {
				FRA = {
					has_tech = Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					FRA = { has_tech = Fighter_1936}
					BUL = { NOT = { has_tech = Fighter_1936 } }
				}
				AND = {
					FRA = { has_tech = Fighter_1940}
					BUL = { NOT = { has_tech = Fighter_1940 } }
				}
				AND = {
					FRA = { has_tech = Fighter_1943}
					BUL = { NOT = { has_tech = Fighter_1943 } }
				}
			}
			NOT = { has_war_with = FRA }
			NOT = { has_country_flag = FRA_fighter_refused }
			
		}
		FRA = { country_event = DH_Bulgaria.6 }	
	}
	option = {# select british plane
		name = DOD_hungary.151.d
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = ENG
				factor = 10
			}
			modifier = {
				ENG = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				ENG = {
					has_tech = Fighter_1936
				}
				add = 10
			}
			modifier = {
				ENG = {
					has_tech = Fighter_1940
				}
				add = 10
			}
			modifier = {
				ENG = {
					has_tech = Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					ENG = { has_tech = Fighter_1936}
					BUL = { NOT = { has_tech = Fighter_1936 } }
				}
				AND = {
					ENG = { has_tech = Fighter_1940}
					BUL = { NOT = { has_tech = Fighter_1940 } }
				}
				AND = {
					ENG = { has_tech = Fighter_1943}
					BUL = { NOT = { has_tech = Fighter_1943 } }
				}
			}
			NOT = { has_war_with = ENG }
			NOT = { has_country_flag = ENG_fighter_refused }
		}
		ENG = { country_event = DH_Bulgaria.6 }	
	}
	option = {# select czech plane
		name = DH_Bulgaria.4.e
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = CZE
				factor = 10
			}
			modifier = {
				CZE = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				CZE = {
					has_tech = Fighter_1936
				}
				add = 10
			}
			modifier = {
				CZE = {
					has_tech = Fighter_1940
				}
				add = 10
			}
			modifier = {
				CZE = {
					has_tech = Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					CZE = { has_tech = Fighter_1936}
					BUL = { NOT = { has_tech = Fighter_1936 } }
				}
				AND = {
					CZE = { has_tech = Fighter_1940}
					BUL = { NOT = { has_tech = Fighter_1940 } }
				}
				AND = {
					CZE = { has_tech = Fighter_1943}
					BUL = { NOT = { has_tech = Fighter_1943 } }
				}
			}
			NOT = { has_war_with = CZE }
			NOT = { has_country_flag = CZE_fighter_refused }
			NOT = { #to prevent overflow issues
				AND = {
					GER = {
						is_available_fighter_BUL = yes
					}
					FRA = {
						is_available_fighter_BUL = yes
					}
					ITA = {
						is_available_fighter_BUL = yes
					}
					ENG = {
						is_available_fighter_BUL = yes
					}
				}
			}
		}
		CZE = { country_event = DH_Bulgaria.6 }	
	}
	option = {# failsafe
		name = DOD_hungary.151.f	
		trigger = {
			OR = {
				has_country_flag = GER_fighter_refused
				has_country_flag = ITA_fighter_refused
				has_country_flag = ENG_fighter_refused
				has_country_flag = FRA_fighter_refused
				has_country_flag = CZE_fighter_refused
			}
		}
		air_experience = 50
	}

}

#Fighter competition - Bulgaria is looking for a heavy fighter
country_event = {
	id = DH_Bulgaria.5
	title = DOD_hungary.152.t
	desc = {
		text = DOD_hungary.152.desc_heavy_fighter
		trigger = {
			NOT = { 
				has_country_flag = GER_heavy_fighter_refused
				has_country_flag = ENG_heavy_fighter_refused
				has_country_flag = CZE_heavy_fighter_refused
				has_country_flag = FRA_heavy_fighter_refused
				has_country_flag = ITA_heavy_fighter_refused
			} 
		}
	}
	desc = {
		text = DOD_hungary.152.desc_heavy_fighter_refused
		trigger = {
			OR = {
		 		has_country_flag = GER_heavy_fighter_refused
				has_country_flag = ENG_heavy_fighter_refused
				has_country_flag = CZE_heavy_fighter_refused
				has_country_flag = FRA_heavy_fighter_refused
				has_country_flag = ITA_heavy_fighter_refused
		 	}
		}
	}
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes

	option = {# Select German Plane
		name = DOD_hungary.151.a
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = GER
				factor = 10
			}
			modifier = {
				GER = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				GER = {
					has_tech = heavy_Fighter_1936
				}
				add = 10
			}
			modifier = {
				GER = {
					has_tech = heavy_Fighter_1940
				}
				add = 10
			}
			modifier = {
				GER = {
					has_tech = heavy_Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					GER = { has_tech = heavy_Fighter_1936}
					BUL = { NOT = { has_tech = heavy_Fighter_1936 } }
				}
				AND = {
					GER = { has_tech = heavy_Fighter_1940}
					BUL = { NOT = { has_tech = heavy_Fighter_1940 } }
				}
				AND = {
					GER = { has_tech = heavy_Fighter_1943}
					BUL = { NOT = { has_tech = heavy_Fighter_1943 } }
				}
			}
			NOT = { has_war_with = GER }
			NOT = { has_country_flag = GER_heavy_fighter_refused }
		}
		GER = { country_event = DH_Bulgaria.7 }	
	}

	option = {# select italian plane
		name = DOD_hungary.151.b
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = ITA
				factor = 10
			}
			modifier = {
				ITA = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				ITA = {
					has_tech = heavy_Fighter_1936
				}
				add = 10
			}
			modifier = {
				ITA = {
					has_tech = heavy_Fighter_1940
				}
				add = 10
			}
			modifier = {
				ITA = {
					has_tech = heavy_Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					ITA = { has_tech = heavy_Fighter_1936}
					BUL = { NOT = { has_tech = heavy_Fighter_1936 } }
				}
				AND = {
					ITA = { has_tech = heavy_Fighter_1940}
					BUL = { NOT = { has_tech = heavy_Fighter_1940 } }
				}
				AND = {
					ITA = { has_tech = heavy_Fighter_1943}
					BUL = { NOT = { has_tech = heavy_Fighter_1943 } }
				}
			}
			NOT = { has_war_with = ITA }
			NOT = { has_country_flag = ITA_heavy_fighter_refused }
		}
		ITA = { country_event = DH_Bulgaria.7 }	
	}

	option = {# select french plane
		name = DOD_hungary.151.c
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = FRA
				factor = 10
			}
			modifier = {
				FRA = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				FRA = {
					has_tech = heavy_Fighter_1936
				}
				add = 10
			}
			modifier = {
				FRA = {
					has_tech = heavy_Fighter_1940
				}
				add = 10
			}
			modifier = {
				FRA = {
					has_tech = heavy_Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					FRA = { has_tech = heavy_Fighter_1936}
					BUL = { NOT = { has_tech = heavy_Fighter_1936 } }
				}
				AND = {
					FRA = { has_tech = heavy_Fighter_1940}
					BUL = { NOT = { has_tech = heavy_Fighter_1940 } }
				}
				AND = {
					FRA = { has_tech = heavy_Fighter_1943}
					BUL = { NOT = { has_tech = heavy_Fighter_1943 } }
				}
			}
			NOT = { has_war_with = FRA }
			NOT = { has_country_flag = FRA_heavy_fighter_refused }
			
		}
		FRA = { country_event = DH_Bulgaria.7 }	
	}
	option = {# select british plane
		name = DOD_hungary.151.d
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = ENG
				factor = 10
			}
			modifier = {
				ENG = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				ENG = {
					has_tech = heavy_Fighter_1936
				}
				add = 10
			}
			modifier = {
				ENG = {
					has_tech = heavy_Fighter_1940
				}
				add = 10
			}
			modifier = {
				ENG = {
					has_tech = heavy_Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					ENG = { has_tech = heavy_Fighter_1936}
					BUL = { NOT = { has_tech = heavy_Fighter_1936 } }
				}
				AND = {
					ENG = { has_tech = heavy_Fighter_1940}
					BUL = { NOT = { has_tech = heavy_Fighter_1940 } }
				}
				AND = {
					ENG = { has_tech = heavy_Fighter_1943}
					BUL = { NOT = { has_tech = heavy_Fighter_1943 } }
				}
			}
			NOT = { has_war_with = ENG }
			NOT = { has_country_flag = ENG_heavy_fighter_refused }
		}
		ENG = { country_event = DH_Bulgaria.7 }	
	}
	option = {# select Czech plane
		name = DH_Bulgaria.5.e
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction_with = CZE
				factor = 10
			}
			modifier = {
				CZE = { has_government = ROOT }
				factor = 2
			}
			modifier = {
				CZE = {
					has_tech = heavy_Fighter_1936
				}
				add = 10
			}
			modifier = {
				CZE = {
					has_tech = heavy_Fighter_1940
				}
				add = 10
			}
			modifier = {
				CZE = {
					has_tech = heavy_Fighter_1943
				}
				add = 10
			}
		}
		trigger = {
			OR = {
				AND = {
					CZE = { has_tech = heavy_Fighter_1936}
					BUL = { NOT = { has_tech = heavy_Fighter_1936 } }
				}
				AND = {
					CZE = { has_tech = heavy_Fighter_1940}
					BUL = { NOT = { has_tech = heavy_Fighter_1940 } }
				}
				AND = {
					CZE = { has_tech = heavy_Fighter_1943}
					BUL = { NOT = { has_tech = heavy_Fighter_1943 } }
				}
			}
			NOT = { has_war_with = CZE }
			NOT = { has_country_flag = CZE_heavy_fighter_refused }
			NOT = { #to prevent overflow issues
				AND = {
					GER = {
						is_available_heavy_fighter_BUL = yes
					}
					FRA = {
						is_available_heavy_fighter_BUL = yes
					}
					ITA = {
						is_available_heavy_fighter_BUL = yes
					}
					ENG = {
						is_available_heavy_fighter_BUL = yes
					}
				}
			}
		}
		CZE = { country_event = DH_Bulgaria.7 }	
	}
	option = {# failsafe
		name = DOD_hungary.151.f	
		trigger = {
			OR = {
				has_country_flag = GER_heavy_fighter_refused
				has_country_flag = ITA_heavy_fighter_refused
				has_country_flag = ENG_heavy_fighter_refused
				has_country_flag = FRA_heavy_fighter_refused
				has_country_flag = CZE_heavy_fighter_refused
			}
		}
		air_experience = 50
	}

}

#fighter competition - light fighter competition winner gets a choice whether to permit license production
country_event = {
	id = DH_Bulgaria.6
	title = DOD_hungary.153.t
	desc = DOD_hungary.153.desc 
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes

	option = {# certainly
		name = DOD_hungary.153.a
		ai_chance = {
			factor = 90
			modifier = {
				has_war = yes
				factor = 0.5
			}
		}
		BUL = { country_event = DH_Bulgaria.8 }
		air_experience = 20
		if = {
			limit = {
				has_tech = Fighter_1936
				NOT = { has_tech = Fighter_1940 }
			}
			create_production_license = {
				target = BUL 
				equipment = {
					type = Fighter_equipment_1936
					
				}
				cost_factor = 0
			}
		}
		if = {
			limit = {
				has_tech = Fighter_1940
				NOT = { has_tech = Fighter_1943 }
			}
			create_production_license = {
				target = BUL 
				equipment = {
					type = Fighter_equipment_1940
				}
				cost_factor = 0
			}
		}
		if = {
			limit = {
				has_tech = Fighter_1943
			}
			create_production_license = {
				target = BUL 
				equipment = {
					type = Fighter_equipment_1943
				}
				cost_factor = 0
			}
		}
	}

	option = {# no
		name = DOD_hungary.153.b
		ai_chance = {
			factor = 10
			modifier = {
				is_in_faction_with = BUL
				factor = 0
			}
		}
		if = {
			limit = { tag = GER }
			BUL = { set_country_flag = GER_fighter_refused }
		}
		if = {
			limit = { tag = ITA }
			BUL = { set_country_flag = ITA_fighter_refused }
		}
		if = {
			limit = { tag = FRA }
			BUL = { set_country_flag = FRA_fighter_refused }
		}
		if = {
			limit = { tag = ENG }
			BUL = { set_country_flag = ENG_fighter_refused }
		}
		if = {
			limit = { tag = CZE }
			BUL = { set_country_flag = CZE_fighter_refused }
		}
		BUL = { country_event = DH_Bulgaria.4 }
		reverse_add_opinion_modifier = { target = BUL modifier = refused_help }
	}
}

#fighter competition - heavy fighter competition winner gets a choice whether to permit license production
country_event = {
	id = DH_Bulgaria.7
	title = DOD_hungary.153.t
	desc = DOD_hungary.154.desc 
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes

	option = {# certainly
		name = DOD_hungary.153.a
		ai_chance = {
			factor = 90
			modifier = {
				has_war = yes
				factor = 0.5
			}
		}
		BUL = { country_event = DH_Bulgaria.8 }
		air_experience = 20
		if = {
			limit = {
				has_tech = heavy_Fighter_1936
				NOT = { has_tech = heavy_Fighter_1940 }
			}
			create_production_license = {
				target = BUL 
				equipment = {
					type = heavy_Fighter_equipment_1936
				}
				cost_factor = 0
			}
		}
		if = {
			limit = {
				has_tech = heavy_Fighter_1940
				NOT = { has_tech = heavy_Fighter_1943 }
			}
			create_production_license = {
				target = BUL 
				equipment = {
					type = heavy_Fighter_equipment_1940	
				}
				cost_factor = 0
			}
		}
		if = {
			limit = {
				has_tech = heavy_Fighter_1943
			}
			create_production_license = {
				target = BUL 
				equipment = {
					type = heavy_Fighter_equipment_1943
				}
				cost_factor = 0
			}
		}
		
	}

	option = {# no
		name = DOD_hungary.153.b
		ai_chance = {
			factor = 10
			modifier = {
				is_in_faction_with = BUL
				factor = 0
			}
		}
		if = {
			limit = { tag = GER }
			BUL = { set_country_flag = GER_heavy_fighter_refused }
		}
		if = {
			limit = { tag = ITA }
			BUL = { set_country_flag = ITA_heavy_fighter_refused }
		}
		if = {
			limit = { tag = FRA }
			BUL = { set_country_flag = FRA_heavy_fighter_refused }
		}
		if = {
			limit = { tag = ENG }
			BUL = { set_country_flag = ENG_heavy_fighter_refused }
		}
		if = {
			limit = { tag = CZE }
			BUL = { set_country_flag = CZE_heavy_fighter_refused }
		}
		BUL = { country_event = DH_Bulgaria.5 }
		reverse_add_opinion_modifier = { target = BUL modifier = refused_help }
	}
}

#fighter competition - license production approved
country_event = {
	id = DH_Bulgaria.8
	title = DOD_hungary.155.t
	desc = DOD_hungary.155.desc 
	picture = GFX_report_event_fighters
	
	is_triggered_only = yes

	option = {# neat
		name = DOD_hungary.155.a
		
	}
}
