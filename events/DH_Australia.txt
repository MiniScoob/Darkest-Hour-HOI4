﻿###########################
# Darkest Hour Election Events : Australia
###########################
add_namespace = DH_Australia_Politics
add_namespace = DH_Australia_Elections

#########################################################################
#  Australian election 1937
#########################################################################
country_event = { 
	id = DH_Australia_Elections.1
	title = DH_Australia_Elections.1.t
	desc = DH_election.1.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	trigger = {
		original_tag = AST
		date > 1937.1.1
		date < 1938.1.1
	}
	
	# Joseph A. Lyons (Conservatives) *
	option = {
		name = DH_Australia_Elections.1.A
		ai_chance = { 
			factor = 51 
		}
		set_party_name = { ideology = democratic long_name = "UAP–Country coalition" name = "UAP-C" }
		create_country_leader = { 
			name = "Joseph A. Lyons" 
			desc = "" 
			picture = "P_D_Joseph_Lyons.tga" 
			expire = "1965.1.1" 
			ideology = social_conservatism 
			traits = { }
		}		
	}

	# John J. Curtin (SocDem)
	option = {
		name = DH_Australia_Elections.1.B
		ai_chance = { 
			factor = 49
			modifier = { 
				is_historical_focus_on = yes 
				factor = 0
			}
		}
		set_party_name = { ideology = democratic long_name = "Labor Party" name = "LP" }
		create_country_leader = {
			name = "John J. Curtin"
			desc = ""
			picture = "P_D_John_Curtin.tga"
			expire = "1945.7.5"
			ideology = social_democracy
			traits = {}
		}	
	}
}

#########################################################################
#  Australian election 1940
#########################################################################
country_event = {
	id = DH_Australia_Elections.2
	title = DH_Australia_Elections.2.t
	desc = DH_election.1.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		original_tag = AST
		date > 1940.1.1
		date < 1941.1.1
	}	
	# Robert Menzies (Conservatives)*
	option = {
		name = DH_Australia_Elections.2.A
		ai_chance = { factor = 51 }
		set_party_name = { ideology = democratic long_name = "UAP–Country coalition" name = "UAP-C" }
		create_country_leader = { 
			name = "Robert Menzies" 
			desc = "" 
			picture = "P_D_Robert_Menzies.tga" 
			expire = "1965.1.1" 
			ideology = social_conservatism 
			traits = { }
		}
	}

	# John J. Curtin (SocDem)
	option = {
		name = DH_Australia_Elections.2.B
		ai_chance = { 
			factor = 49 
			modifier = { 
				is_historical_focus_on = yes 
				factor = 0
			}
		}  
		set_party_name = { ideology = democratic long_name = "Labor Party" name = "LP" }
		create_country_leader = {
			name = "John J. Curtin"
			desc = ""
			picture = "P_D_John_Curtin.tga"
			expire = "1945.7.5"
			ideology = social_democracy
			traits = {}
		}
	}
}
#########################################################################
#  Australian election 1943
#########################################################################
country_event = {
	id = DH_Australia_Elections.3
	title = DH_Australia_Elections.3.t
	desc = DH_election.1.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		original_tag = AST
		date > 1943.1.1
		date < 1944.1.1
	}

	# Arthur Fadden (Conservatism)
	option = {
		name = DH_Australia_Elections.3.A
		ai_chance = { 
			factor = 42 
			modifier = { 
				is_historical_focus_on = yes 
				factor = 0
			}			
		}
		set_party_name = { ideology = democratic long_name = "UAP–Country coalition" name = "UAP-C" }
		create_country_leader = { 
			name = "Arthur W. Fadden" 
			desc = "" 
			picture = "P_D_Arthur_Fadden.tga" 
			expire = "1965.1.1" 
			ideology = social_conservatism 
			traits = { }
		}		
	}

	# John J. Curtin (SocDem)*
	option = {
		name = DH_Australia_Elections.3.B
		ai_chance = { factor = 58 }
		set_party_name = { ideology = democratic long_name = "Labor Party" name = "LP" }
		create_country_leader = {
			name = "John J. Curtin"
			desc = ""
			picture = "P_D_John_Curtin.tga"
			expire = "1945.7.5"
			ideology = social_democracy
			traits = {}
		}		
	}
}
#########################################################################
#  Australian election 1946
#########################################################################
country_event = {
	id = DH_Australia_Elections.4
	title = DH_Australia_Elections.4.t
	desc = DH_election.1.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes
	is_triggered_only = yes	
	trigger = {
		original_tag = AST
		date > 1946.1.1
		date < 1947.1.1	
	}

	# Arthur Fadden (Conservatism)
	option = {
		name = DH_Australia_Elections.3.A
		ai_chance = { 
			factor = 46 
			modifier = { 
				is_historical_focus_on = yes 
				factor = 0
			}			
		}
		set_party_name = { ideology = democratic long_name = "Liberal–Country coalition" name = "L-C" }
		create_country_leader = { 
			name = "Arthur W. Fadden" 
			desc = "" 
			picture = "P_D_Arthur_Fadden.tga" 
			expire = "1965.1.1" 
			ideology = social_conservatism 
			traits = { }
		}		
	}
	# Ben Chifley (SocDem) *
	option = {
		name = DH_Australia_Elections.4.B
		ai_chance = { factor = 54 }
		set_party_name = { ideology = democratic long_name = "Labor Party" name = "LP" }
		create_country_leader = {
			name = "Ben Chifley"
			desc = ""
			picture = "P_D_Ben_Chifley.tga"
			expire = "1945.7.5"
			ideology = social_democracy
			traits = {}
		}		
	}
}

#########################################################################
#  PM Robert Menzies resigns
#########################################################################
country_event = { 
	id = DH_Australia_Politics.1
	title = DH_Australia_Politics.1.t
	desc = DH_Australia_Politics.1.d
	picture = GFX_DH_Robert_Menzies_Resign
	
	fire_only_once = yes

	trigger = {
		original_tag = AST
		is_democratic = yes
		has_country_leader = {
		    name = "Robert Menzies"
		    ruling_only = yes
		}
		date > 1941.8.28
		date < 1941.9.28		
	}

	# Signed, Sir Robert Gordon Menzies
	option = {
		name = DH_Australia_Politics.1.A
		retire_country_leader = yes
		create_country_leader = { 
			name = "Arthur W. Fadden" 
			desc = "" 
			picture = "P_D_Arthur_Fadden.tga" 
			expire = "1965.1.1" 
			ideology = social_conservatism 
			traits = { }
		}
		set_party_name = { ideology = democratic long_name = "Australian Country Party" name = "ACP" }
	}	
}

#########################################################################
#  John Curtin sworn in as Prime Minister
#########################################################################
country_event = { 
	id = DH_Australia_Politics.2
	title = DH_Australia_Politics.2.t
	desc = DH_Australia_Politics.2.d
	picture = GFX_DH_John_Curtin
	
	fire_only_once = yes
	trigger = {
		original_tag = AST
		is_democratic = yes
		date > 1941.10.7	
	}

	option = {
		name = DH_Australia_Politics.2.A
		set_party_name = { ideology = democratic long_name = "Labor Party" name = "LP" }
		create_country_leader = {
			name = "John J. Curtin"
			desc = ""
			picture = "P_D_John_Curtin.tga"
			expire = "1945.7.5"
			ideology = social_democracy
			traits = {}
		}		
	}	
}

#########################################################################
#  John Curtain dies at the age of 60
#########################################################################
country_event = { 
	id = DH_Australia_Politics.3
	title = DH_Australia_Politics.3.t
	desc = DH_Australia_Politics.3.d
	picture = GFX_DH_John_Curtin_Funeral
	
	fire_only_once = yes
	trigger = {
		original_tag = AST
		date > 1945.7.5
		has_country_leader = {
		    name = "John J. Curtin"
		    ruling_only = yes
		}		
	}

	# May his soul rest in peace
	option = {
		name = DH_Australia_Politics.3.A
		set_party_name = { ideology = democratic long_name = "Labor Party" name = "LP" }
		create_country_leader = { 
			name = "Frank Forde" 
			desc = "" 
			picture = "P_D_Frank_Forde.tga" 
			expire = "1965.1.1" 
			ideology = social_democracy 
			traits = {} 
		}		
	}
}