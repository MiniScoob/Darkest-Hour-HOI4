Modify_Production_Quotas_idea = {
    if = {
        limit = {
            has_idea = SOV_Production_Quotas
        }
        swap_ideas = {
            remove_idea = SOV_Production_Quotas
            add_idea = SOV_Production_Quotas_1
        }
    }
    else_if = {
        limit = {
            has_idea = SOV_Production_Quotas_1
        }
        swap_ideas = {
            remove_idea = SOV_Production_Quotas_1
            add_idea = SOV_Production_Quotas_2
        }
    }
    else_if = {
        limit = {
            has_idea = SOV_Production_Quotas_2
        }
        swap_ideas = {
            remove_idea = SOV_Production_Quotas_2
            add_idea = SOV_Production_Quotas_3
        }
    }
}