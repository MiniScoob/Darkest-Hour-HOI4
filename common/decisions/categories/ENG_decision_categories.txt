#########################################################################
#  British Decision Categories
#########################################################################
###########################
# Invest in the Dominions
###########################
ENG_invest_dominions_category = {
	icon = placeholder

	picture = placeholder
	
	visible = { 
	    #has_completed_focus =ENG_Invest_in_our_Dominions
	}

	allowed = {
		original_tag = ENG 
	}
}
