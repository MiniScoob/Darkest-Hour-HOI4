#####################
##### DH Spain ######
#####################

SPR_intervene_civil_war = {
	allowed = {
		always = yes
	}

	visible = {
		SPR = { has_war_with = SPA }
		OR = {
			is_major = yes
			tag = GER
			tag = ITA
			tag = POR
			tag = ENG
			tag = FRA
			tag = MEX
			tag = SOV
		}
	}
}