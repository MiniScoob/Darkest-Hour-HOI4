#########################################################################
#  Turkish Diplomactic Decisions
#########################################################################
foreign_politics = {
###########################
# Ask for Hatay
###########################
	TUR_Ask_for_Hatay = {
		icon = ger_reichskommissariats
		fire_only_once = yes
		visible = {
			original_tag = TUR
			is_puppet = no
			date > 1939.1.1
			FRA = { exists = yes }
			FRA = { controls_state = 803 }			
		}
		available = {
			FRA = { controls_state = 803 }
			NOT = {
				OR = {
					country_exists = SYR
					has_war_with = FRA
				}
			}		
		}

		ai_will_do = {
			factor = 0
			modifier = {
				add = 100
				date > 1939.6.29 #29 June 1939
			}
		}

		complete_effect = {
			FRA = { country_event = { id = DH_France_Turkey.1 hours = 6 } }
		}
	}	
}