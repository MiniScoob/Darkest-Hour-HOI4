####################################################
## Factions
#####################################################

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

# Remain in the Axis
Remain_in_the_Axis = {
	enable = {
		OR = {
			has_government = fascist
			has_government = authoritarian
		}
		is_in_faction_with = GER
	}
	abort = {
		NOT = {
			OR = {
				has_government = fascist
				has_government = authoritarian
			}
			is_in_faction_with = GER
		}
	}

	ai_strategy = {
		type = alliance
		id = "GER"
		value = 1000
	}
}
# Remain in the Allies
Remain_in_the_Allies = {
	enable = {
		is_democratic = yes
		is_in_faction_with = ENG
	}
	abort = {
		NOT = {
			is_democratic = yes
			is_in_faction_with = ENG
		}
	}

	ai_strategy = {
		type = alliance
		id = "ENG"
		value = 1000
	}
}
