on_actions = {
	on_startup = {
		effect = {
			every_country = {
				set_variable = { money = 0 }
				add_dynamic_modifier = {
					modifier = inflation_dynamic_modifier
				}
			}
		}
	}
	
	on_daily = {
		effect = {
			set_temp_variable = { gained_money = 0 }
			if = {
				limit = {
					calculate_money_balance = yes # calculates the money
					OR = {
						check_variable = { money < 2000000 }
						NOT = { check_variable = { total_balance_temp > 0 } }
					}
					OR = {
						check_variable = { money > -2000000 }
						NOT = { check_variable = { total_balance_temp < 0 } }
					}
				}
				set_temp_variable = { gained_money = total_balance_temp }
				add_to_variable = { money = gained_money }
				if = {
					limit = {
						check_variable = { num_of_loans > 0 }
					}
					for_loop_effect = {
						start = 1
						end = 21
						value = v
						meta_effect = {
							text = {
								if = {
									limit = {
										check_variable = { current_loan_[LOAN] > 0 }
									}
									subtract_from_variable = { current_loan_[LOAN] = 1 }
									if = {
										limit = {
											NOT = { check_variable = { current_loan_[LOAN] > 0 } }
										}
										set_variable = { current_loan_[LOAN] = 0 }
										subtract_from_variable = { num_of_loans = 1 }
									}
								}
							}
							LOAN = "[?v]"
						}
					}
				}
			}
			if = {
				limit = {
					check_variable = { money > 2000000 }
				}
				set_variable = { money = 2000000 }
			}
			else_if = {
				limit = {
					check_variable = { money < -2000000 }
				}
				set_variable = { money = -2000000 }
			}
			
			if = {
				limit = {
					calculate_inflation_change = yes # calculates the inflation change
				}
				add_to_variable = { current_inflation = daily_inflation_change }
				set_variable = { inflation_effect_var = current_inflation }
				divide_variable = { inflation_effect_var = -2 }
			}
			if = {
				limit = {
					check_variable = { current_inflation < 0 }
				}
				set_variable = { current_inflation = 0 }
			}
			else_if = {
				limit = {
					check_variable = { current_inflation > 20000 }
				}
				set_variable = { current_inflation = 20000 }
			}
		}
	}
}