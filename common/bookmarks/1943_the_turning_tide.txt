bookmarks = {
	bookmark = {
		name = "THE_TURNING_TIDE_name"
		desc = "THE_TURNING_TIDE_desc"
		date = 1943.7.26.12
		picture = "GFX_select_date_1943"
		default_country = "GER"
		default = yes
		
		"FRA"={
			history = "FRA_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				FRA_victors_of_wwi
				FRA_disjointed_government
				FRA_protected_by_the_maginot_line
			}
			focuses = {
			}
		}
		"USA"={
			history = "USA_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				home_of_the_free
				great_depression
				undisturbed_isolation
			}
			focuses = {

			}
		}
		"ENG"={
			history = "ENG_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				stiff_upper_lip
				ENG_the_war_to_end_all_wars
				george_v
			}
			focuses = {

			}
		}

		"GER"={
			history = "GER_GATHERING_STORM_DESC"
			ideology = fascist
			ideas = {
				sour_loser
				general_staff
				GER_mefo_bills_1		
			}
			focuses = {

			}
		}
		"ITA"={
			history = "ITA_GATHERING_STORM_DESC"
			ideology = fascist
			ideas = {
				vittoria_mutilata
				victor_emmanuel
			}	
			focuses = {

			}		
		}
		"JAP"={
			history = "JAP_GATHERING_STORM_DESC"
			ideology = fascist
			ideas = {
				state_shintoism
				JAP_zaibatsus
			}	
			focuses = {

			}
		}
		"SOV"={
			history = "SOV_GATHERING_STORM_DESC"
			ideology = communist
			ideas = {
				trotskyite_plot
				home_of_revolution
			}
			focuses = {

			}	
		}

		"---"={
			history = "OTHER_GATHERING_STORM_DESC"
		}


		effect = {
			randomize_weather = 22345 # <- Obligatory in every bookmark !
			#123 = { rain_light = yes }
		}
	}
}
