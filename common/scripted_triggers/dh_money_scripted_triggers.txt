has_specific_amount_of_money = {
	custom_trigger_tooltip = {
		tooltip = HAS_SPECIFIC_AMOUNT_OF_MONEY
		NOT = { check_variable = { money < amount_of_money_to_check } }
	}
}

has_less_than_specific_amount_of_money = {
	custom_trigger_tooltip = {
		tooltip = HAS_LESS_THAN_SPECIFIC_AMOUNT_OF_MONEY
		check_variable = { money < amount_of_money_to_check }
	}
}

calculate_money_balance = {
	set_temp_variable = { total_balance_temp = 0 }
	
	# Income:
	calculate_money_income = yes
	
	# Expenses:
	calculate_money_expenses = yes
	
	# Balance:
	add_to_temp_variable = { total_balance_temp = total_income_temp }
	add_to_temp_variable = { total_balance_temp = total_expenses_temp }
}

calculate_inflation_change = {
	set_temp_variable = { daily_inflation_change = 0 }
	
	# Bankruptcy:
	if = {
		limit = {
			has_idea = bankruptcy
		}
		subtract_from_temp_variable = { daily_inflation_change = 0.01 }
	}
	# Financial Crisis:
	if = {
		limit = {
			NOT = { check_variable = { great_depression_income_efficiency = 0 } }
		}
		add_to_temp_variable = { daily_inflation_change = 0.001 }
	}
	# Deflation Measurements:
	set_temp_variable = { reducing_inflation_temp = reducing_inflation }
	divide_temp_variable = { reducing_inflation_temp = 100 }
	add_to_temp_variable = { daily_inflation_change = reducing_inflation_temp }
	# Print Money:
	set_temp_variable = { printing_money_temp = printing_money }
	divide_temp_variable = { printing_money_temp = 100 }
	add_to_temp_variable = { daily_inflation_change = printing_money_temp }
}

calculate_money_income = {
	set_temp_variable = { total_income_temp = 0 }
	
	# Taxes
	set_temp_variable = { income_from_our_taxes = 0 }
	set_temp_variable = { income_from_our_taxes_core = 0 }
	set_temp_variable = { income_from_our_taxes_non_core = 0 }
	all_owned_state = {
		if = {
			limit = {
				is_core_of = PREV
			}
			add_to_temp_variable = { income_from_our_taxes_core = infrastructure_level }
		}
		else = {
			add_to_temp_variable = { income_from_our_taxes_non_core = infrastructure_level }
		}
	}
	multiply_temp_variable = { income_from_our_taxes_core = 0.05 }
	multiply_temp_variable = { income_from_our_taxes_non_core = 0.01 }
	add_to_temp_variable = { income_from_our_taxes = income_from_our_taxes_core }
	add_to_temp_variable = { income_from_our_taxes = income_from_our_taxes_non_core }
	
	set_temp_variable = { tax_efficiency_modifier = 1 }
	## Tax Modifiers:
	if = {
		limit = {
			check_variable = { tax_efficiency_modifier < 0 }
		}
		set_temp_variable = { tax_efficiency_modifier = 0 }
	}
	multiply_temp_variable = { income_from_our_taxes = tax_efficiency_modifier }
	add_to_temp_variable = { total_income_temp = income_from_our_taxes }
	
	# Consumer Goods Factories:
	set_temp_variable = { income_from_our_consumer_goods_factories = num_of_civilian_factories }
	multiply_temp_variable = { income_from_our_consumer_goods_factories = modifier@consumer_goods_factor }
	round_temp_variable = income_from_our_consumer_goods_factories
	multiply_temp_variable = { income_from_our_consumer_goods_factories = 0.2 }
	add_to_temp_variable = { total_income_temp = income_from_our_consumer_goods_factories }
	
	# Free Civilian Factories:
	set_temp_variable = { income_from_our_free_civilian_factories = num_of_civilian_factories_available_for_projects }
	multiply_temp_variable = { income_from_our_free_civilian_factories = 0.05 }
	add_to_temp_variable = { total_income_temp = income_from_our_free_civilian_factories }
	
	# Income Affecting Modifiers:
	calculate_money_income_modifiers = yes
}

calculate_money_income_modifiers = {
	set_temp_variable = { income_modifier = 0 }
	
	# Inflation Reduction
	add_to_temp_variable = { income_modifier = reducing_inflation }
	# Great Depression
	if = {
		limit = {
			NOT = { check_variable = { great_depression_income_efficiency = 0 } }
		}
		add_to_temp_variable = { income_modifier = great_depression_income_efficiency }
	}
	
	# Calculation
	if = {
		limit = {
			check_variable = { income_modifier < -1 }
		}
		set_temp_variable = { income_modifier = -1 }
	}
	set_temp_variable = { income_modifier_calculation = 1 }
	add_to_temp_variable = { income_modifier_calculation = income_modifier }
	multiply_temp_variable = { total_income_temp = income_modifier_calculation }
}

calculate_money_expenses = {
	set_temp_variable = { total_expenses_temp = 0 }
	
	# Used Military Factories:
	set_temp_variable = { income_from_our_used_mil_factories = num_of_military_factories }
	subtract_from_temp_variable = { income_from_our_used_mil_factories = num_of_available_military_factories }
	round_temp_variable = income_from_our_used_mil_factories
	multiply_temp_variable = { income_from_our_used_mil_factories = -0.01 }
	add_to_temp_variable = { total_expenses_temp = income_from_our_used_mil_factories }	
	
	# Loans:
	calculate_money_loan_interests = yes
	# Expenses Affecting Modifiers:
	calculate_money_expenses_modifiers = yes
}

calculate_money_loan_interests = {
	set_temp_variable = { income_from_loans = 0 }
	
	if = {
		limit = {
			NOT = { check_variable = { current_loan_1 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_2 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_3 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_4 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_5 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_6 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_7 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_8 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_9 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_10 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_11 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_12 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_13 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_14 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_15 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_16 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_17 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_18 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_19 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	if = {
		limit = {
			NOT = { check_variable = { current_loan_20 = 0 } }
		}
		add_to_temp_variable = { income_from_loans = 1 }
	}
	multiply_temp_variable = { income_from_loans = -1 }
	add_to_temp_variable = { total_expenses_temp = income_from_loans }
}

calculate_money_expenses_modifiers = {
	set_temp_variable = { expenses_modifier = 0 }
	
	# Inflation
	add_to_temp_variable = { expenses_modifier = current_inflation }
	
	# Calculation
	if = {
		limit = {
			check_variable = { expenses_modifier < -1 }
		}
		set_temp_variable = { expenses_modifier = -1 }
	}
	set_temp_variable = { expenses_modifier_calculation = 1 }
	add_to_temp_variable = { expenses_modifier_calculation = expenses_modifier }
	multiply_temp_variable = { total_expenses_temp = expenses_modifier_calculation }
}

calculate_loan_size = {
	calculate_money_income = yes
	set_temp_variable = { loan_size = total_income_temp }
	multiply_temp_variable = { loan_size = 100 }
	round_temp_variable = loan_size
	if = {
		limit = {
			check_variable = { loan_size > 10000 }
		}
		set_temp_variable = { loan_size = 10000 }
	}
	else_if = {
		limit = {
			check_variable = { loan_size < 500 }
		}
		set_temp_variable = { loan_size = 500 }
	}
	set_temp_variable = { loan_extra_money_to_pay = loan_size }
	divide_temp_variable = { loan_extra_money_to_pay = 0.8 }
	subtract_from_temp_variable = { loan_extra_money_to_pay = loan_size }
	round_temp_variable = loan_extra_money_to_pay
	set_temp_variable = { total_loan_size = loan_size }
	add_to_temp_variable = { total_loan_size = loan_extra_money_to_pay }
}