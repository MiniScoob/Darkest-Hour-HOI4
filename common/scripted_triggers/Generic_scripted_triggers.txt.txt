#scripted trigger for FROM(generic)

is_available_light_tank_generic = {
	OR = {
		AND = {
			has_tech = Light_Tank_1936
			FROM = {
				NOT = { has_tech = Light_Tank_1936 }
			}
		}
		AND = {
			has_tech = Light_Tank_1939
			FROM = {
				NOT = { has_tech = Light_Tank_1939 }
			}
		}
		AND = {
			has_tech = Light_Tank_1943
			FROM = {
				NOT = { has_tech = Light_Tank_1943 }
			}
		}
	}
	NOT = {
		has_war_with = FROM
	}
}

is_available_heavy_tank_generic = {
	OR = {
		AND = {
			has_tech = Heavy_Tank_1939
			FROM = {
				NOT = { has_tech = Heavy_Tank_1939 }
			}
		}
		AND = {
			has_tech = Heavy_Tank_1941
			FROM = {
				NOT = { has_tech = Heavy_Tank_1941 }
			}
		}
		AND = {
			has_tech = Heavy_Tank_1943
			FROM = {
				NOT = { has_tech = Heavy_Tank_1943 }
			}
		}
	}
	NOT = {
		has_war_with = FROM
	}
}

is_available_bb_generic = {
	OR = {
		AND = {
			has_tech = BB_1933
			FROM = {
				NOT = { has_tech = BB_1933 }
			}
		}
		AND = {
			has_tech = BB_1940
			FROM = {
				NOT = { has_tech = BB_1940 }
			}
		}
		AND = {
			has_tech = BB_1950
			FROM = {
				NOT = { has_tech = BB_1950 }
			}
		}
	}
	NOT = {
		has_war_with = FROM
	}
}

is_available_bc_generic = {
	OR = {
		AND = {
			has_tech = BC_1933
			FROM = {
				NOT = { has_tech = BC_1933 }
			}
		}
		AND = {
			has_tech = BC_1940
			FROM = {
				NOT = { has_tech = BC_1940 }
			}
		}
		AND = {
			has_tech = BC_1950
			FROM = {
				NOT = { has_tech = BC_1950 }
			}
		}
	}
	NOT = {
		has_war_with = FROM
	}
}