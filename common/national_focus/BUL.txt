focus_tree = {
	id = BUL
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = BUL
		}
	}
	default = no
	focus = {
		id = BUL_The_Second_National_Crisis
		icon = GFX_focus_CZE_german_puppet
		cost = 20.00
		x = 9
		y = 0
		completion_reward = {
			add_stability = 0.05
		}
	}
	focus = {
		id = BUL_Recovery_from_the_Great_Depression
		icon = GFX_focus_CZE_german_puppet
		cost = 20.00
		x = 5
		y = 0
		completion_reward = {
			add_political_power = 100
		}
	}
	focus = {
		id = BUL_Civilian_Projects
		icon = GFX_goal_continuous_non_factory_construct
		cost = 20.00
		prerequisite = {
			focus = BUL_Recovery_from_the_Great_Depression
		}
		x = 1
		y = 1
		completion_reward = {
			add_tech_bonus = {
			    bonus = 0.5
			    uses = 1
			    category = industry
			    category = electronics
			}
			48 = {
				add_building_construction = {
				    type = industrial_complex
				    level = 1
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Land_Redistribution
		icon = GFX_GENERIC_Draw_Borders
		cost = 20.00
		prerequisite = {
			focus = BUL_Civilian_Projects
		}
		x = 1
		y = 2
		completion_reward = {
			48 = {
				add_building_construction = {
				    type = industrial_complex
				    level = 1
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Increase_Exports
		icon = GFX_goal_generic_intelligence_exchange
		cost = 20.00
		prerequisite = {
			focus = BUL_Land_Redistribution
		}
		x = 1
		y = 3
		completion_reward = {
			add_ideas = export_focus
		}
	}
	focus = {
		id = BUL_Satisfy_the_Tobacco_Workers
		icon = GFX_focus_generic_concessions
		cost = 20.00
		prerequisite = {
			focus = BUL_Recovery_from_the_Great_Depression
		}
		x = 3
		y = 1
		completion_reward = {
			swap_ideas = {
				remove_idea = BUL_Recovering_from_the_Great_Depression
				add_idea = BUL_1st_Great_Depression_Reform
			}
		}
	}
	focus = {
		id = BUL_Agriculture_Subsidies
		icon = GFX_GENERIC_Money
		cost = 20.00
		prerequisite = {
			focus = BUL_Satisfy_the_Tobacco_Workers
		}
		x = 3
		y = 2
		completion_reward = {
			swap_ideas = {
				remove_idea = BUL_1st_Great_Depression_Reform
				add_idea = BUL_2nd_Great_Depression_Reform
			}
		}
	}
	focus = {
		id = BUL_Diversify_Crop_Production
		icon = GFX_GENERIC_Support_for_Agriculture
		cost = 20.00
		prerequisite = {
			focus = BUL_Agriculture_Subsidies
		}
		x = 3
		y = 3
		completion_reward = {
			swap_ideas = {
				remove_idea = BUL_2nd_Great_Depression_Reform
				add_idea = BUL_3rd_Great_Depression_Reform
			}
		}
	}
	focus = {
		id = BUL_Infrastructure_Buildup
		icon = GFX_goal_generic_construct_infrastructure
		cost = 20.00
		prerequisite = {
			focus = BUL_Recovery_from_the_Great_Depression
		}
		x = 5
		y = 1
		completion_reward = {
			48 = {
				add_building_construction = {
				    type = infrastructure
				    level = 2
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Connect_the_Cities
		icon = GFX_goal_generic_construct_civ_factory
		cost = 20.00
		prerequisite = {
			focus = BUL_Infrastructure_Buildup
		}
		x = 5
		y = 2
		completion_reward = {
			212 = {
				add_building_construction = {
				    type = infrastructure
				    level = 2
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Expand_Shiping_Lanes
		icon = GFX_goal_continuous_naval_production
		cost = 20.00
		prerequisite = {
			focus = BUL_Connect_the_Cities
		}
		x = 5
		y = 3
		completion_reward = {
			add_timed_idea = {
			    idea = BUL_Expanded_Shiping_Lanes
			    days = 365
			}
		}
	}
	focus = {
		id = BUL_Full_Recovery_from_the_Crisis
		icon = GFX_focus_generic_support_the_left_right
		cost = 20.00
		prerequisite = {
			focus = BUL_Diversify_Crop_Production
		}
		prerequisite = {
			focus = BUL_Increase_Exports
		}
		prerequisite = {
			focus = BUL_Expand_Shiping_Lanes
		}
		x = 3
		y = 4
		completion_reward = {
			remove_ideas = BUL_Recovering_from_the_Great_Depression
			212 = {
				add_building_construction = {
				    type = industrial_complex
				    level = 1
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Rearmament_Projects
		icon = GFX_focus_secret_rearmament
		cost = 20.00
		prerequisite = {
			focus = BUL_The_Second_National_Crisis
			focus = BUL_Recovery_from_the_Great_Depression
		}
		x = 7
		y = 1
		completion_reward = {
			212 = {
				add_building_construction = {
				    type = arms_factory
				    level = 1
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Invest_in_Arsenal_AD
		icon = GFX_goal_generic_construct_mil_factory
		cost = 20.00
		prerequisite = {
			focus = BUL_Rearmament_Projects
		}
		x = 7
		y = 2
		completion_reward = {
			212 = {
				add_building_construction = {
				    type = arms_factory
				    level = 1
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Increase_Military_Budget
		icon = GFX_focus_generic_tank_production
		cost = 20.00
		prerequisite = {
			focus = BUL_Invest_in_Arsenal_AD
		}
		x = 7
		y = 3
		completion_reward = {
			212 = {
				add_building_construction = {
				    type = arms_factory
				    level = 1
				    instant_build = yes
				}
			}
			48 = {
				add_building_construction = {
				    type = arms_factory
				    level = 1
				    instant_build = yes
				}
			}			
		}
	}
	focus = {
		id = BUL_Bulgarian_Land_Forces
		icon = GFX_BUL_Bulgarian_Army
		cost = 20.00
		prerequisite = {
			focus = BUL_The_Second_National_Crisis
		}
		x = 9
		y = 1
		completion_reward = {
			army_experience = 10
			add_tech_bonus = {
			    bonus = 0.3
			    uses = 1
			    category = infantry_weapons
			}		
		}
	}
	focus = {
		id = BUL_Acquire_Foreign_Licenses
		icon = GFX_focus_generic_combined_arms
		cost = 20.00
		prerequisite = {
			focus = BUL_Bulgarian_Land_Forces
		}
		x = 9
		y = 2
		completion_reward = {
			ITA = { country_event = DH_Bulgaria.1 }	
			add_ideas = BUL_tank_licenses
		}
	}
	focus = {
		id = BUL_Military_Exercices
		icon = GFX_focus_generic_little_entente
		cost = 20.00
		prerequisite = {
			focus = BUL_Acquire_Foreign_Licenses
		}
		x = 9
		y = 3
		completion_reward = {
			army_experience = 10
			add_tech_bonus = {
			    bonus = 0.5
			    uses = 1
			    category = land_doctrine
			}		
		}
	}
	focus = {
		id = BUL_Bulgarian_Navy
		icon = GFX_BUL_Bulgarian_Navy
		cost = 20.00
		prerequisite = {
			focus = BUL_The_Second_National_Crisis
		}
		x = 11
		y = 1
		completion_reward = {
			navy_experience = 10
			211 = {
				add_building_construction = {
				    type = dockyard
				    level = 1
				    instant_build = yes
				}
			}
			add_tech_bonus = {
			    bonus = 0.3
			    uses = 1
			    category = naval_doctrine
			}						
		}
	}
	focus = {
		id = BUL_New_Torpedo_Boats
		icon = GFX_GENERIC_Torpedo_Boat
		cost = 20.00
		prerequisite = {
			focus = BUL_Bulgarian_Navy
		}
		x = 11
		y = 2
		completion_reward = {
			add_tech_bonus = {
			    bonus = 0.5
			    uses = 1
			    category = dd_tech
			}
		}
	}
	focus = {
		id = BUL_New_Dockyards
		icon = GFX_goal_generic_construct_naval_dockyard
		cost = 20.00
		prerequisite = {
			focus = BUL_New_Torpedo_Boats
		}
		x = 11
		y = 3
		completion_reward = {
			211 = {
				add_building_construction = {
				    type = dockyard
				    level = 2
				    instant_build = yes
				}
			}						
		}
	}
	focus = {
		id = BUL_The_New_Golden_Age
		icon = GFX_BUL_Greater_Bulgaria
		cost = 20.00
		prerequisite = {
			focus = BUL_Increase_Military_Budget
		}
		prerequisite = {
			focus = BUL_Full_Recovery_from_the_Crisis
		}
		prerequisite = {
			focus = BUL_Na_Nozh
		}
		x = 7
		y = 5
		completion_reward = {
			add_stability = 0.1
			add_political_power = 150
		}
	}
	focus = {
		id = BUL_Na_Nozh
		icon = GFX_BUL_Na_Nozh
		cost = 20.00
		prerequisite = {
			focus = BUL_Military_Exercices
		}
		prerequisite = {
			focus = BUL_New_Dockyards
		}
		prerequisite = {
			focus = BUL_Expand_our_Airfields
		}
		x = 11
		y = 4
		completion_reward = {
			add_ideas = BUL_Na_Nozh
			add_war_support = 0.1
		}
	}
	focus = {
		id = BUL_Bulgarian_Airforce
		icon = GFX_BUL_Bulgarian_Air_Force
		cost = 20.00
		prerequisite = {
			focus = BUL_The_Second_National_Crisis
		}
		x = 13
		y = 1
		completion_reward = {
			air_experience  = 10
			48 = {
				add_building_construction = {
				    type = air_base
				    level = 1
				    instant_build = yes
				}
			}
		}
	}
	focus = {
		id = BUL_Import_Foreign_Aircraft
		icon = GFX_focus_chi_flying_tigers
		cost = 20.00
		prerequisite = {
			focus = BUL_Bulgarian_Airforce
		}
		available = {
			NOT = {
				has_tech = Fighter_1943
				has_tech = heavy_Fighter_1943
			}
			OR = {
				GER = {
					custom_trigger_tooltip = {
						tooltip = fighter_tt
						OR = {
							is_available_fighter_BUL = yes
							is_available_heavy_fighter_BUL = yes
						}
					}
				}
				FRA = {
					custom_trigger_tooltip = {
						tooltip = fighter_tt
						OR = {
							is_available_fighter_BUL = yes
							is_available_heavy_fighter_BUL = yes
						}
					}
				}
				ITA = {
					custom_trigger_tooltip = {
						tooltip = fighter_tt
						OR = {
							is_available_fighter_BUL = yes
							is_available_heavy_fighter_BUL = yes
						}
					}
				}
				ENG = {
					custom_trigger_tooltip = {
						tooltip = fighter_tt
						OR = {
							is_available_fighter_BUL = yes
							is_available_heavy_fighter_BUL = yes
						}
					}
				}
				CZE = {
					custom_trigger_tooltip = {
						tooltip = fighter_tt
						OR = {
							is_available_fighter_BUL = yes
							is_available_heavy_fighter_BUL = yes
						}
					}
				}
			}
		}
		x = 13
		y = 2
		completion_reward = {
			country_event = DH_Bulgaria.3
		}
	}
	focus = {
		id = BUL_Expand_our_Airfields
		icon = GFX_focus_YUG_ikarus
		cost = 20.00
		prerequisite = {
			focus = BUL_Import_Foreign_Aircraft
		}
		x = 13
		y = 3
		completion_reward = {
			212 = {
				add_building_construction = {
				    type = air_base
				    level = 2
				    instant_build = yes
				}
			}
		}
	}
}
