focus_tree = {
	id = SHX_focus
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SHX
		}
	}
	default = no
    continuous_focus_position = { x = 10 y = 0 }
	focus = {
		id = SHX_Dummy
		icon = GFX_focus_generic_industry_2
		cost = 10.00
		x = 3
		y = 0

		available_if_capitulated = yes
		available = {
			always = no
		}
		completion_reward = {
			add_tech_bonus = {
				bonus = 0.5
				uses = 1
				category = industry
			}
		}
		
	}
}