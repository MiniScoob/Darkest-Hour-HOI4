autonomy_state = {
	id = autonomy_colony
	
	is_puppet = yes
	
	min_freedom_level = 0.50
	
	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
	}
	
	modifier = {
		autonomy_manpower_share = 0.7
		
		extra_trade_to_overlord_factor = 0.5
		overlord_trade_cost_factor = -0.5
		can_master_build_for_us = 1

		research_sharing_per_country_bonus_factor = -0.5
	}
	
	ai_subject_wants_higher = {
		factor = 1.0
	}
	
	ai_overlord_wants_lower = {
		factor = 1.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		### NOT Commie
		NOT = { 
			has_government = communist
		}		
		### NOT The German Reich
		NOT = { 
			OVERLORD = { 
				original_tag = GER 
				OR = { has_government = fascist }
			} 
		}	
		### NOT The Empire of Japan
		NOT = { 
			OVERLORD = { 
				original_tag = JAP 
				OR = { has_government = fascist has_government = authoritarian }
			} 
		}
		### NOT The Empire of Manchucko
		NOT = { 
			OVERLORD = { 
				original_tag = MAN 
				OR = { has_government = fascist has_government = authoritarian }				
			} 
		}
	}
	
	can_take_level = {
		#trigger here
	}

	can_lose_level = {
		#trigger here
	}
}
