ideas = {
#################################################
### Research Teams
#################################################
    Research_Team = {
        # Frantisek Krizik (Skill : 5)
        CZE_Frantisek_Krizik = {
            picture = CZE_Frantisek_Krizik
            allowed = { original_tag = CZE }
            visible = {
                date > 1900.1.1
                date < 1942.1.1
            }
            research_bonus = {
                mechanics = 0.05
                electronics = 0.05
                industrial_engineering = 0.05
                technical_efficiency = 0.05
            }
            traits = {  }
        }
        # Skoda (Skill : 8)
        CZE_Skoda = {
            picture = CZE_Skoda
            allowed = { original_tag = CZE }
            visible = {
                date > 1900.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.08
                industrial_engineering = 0.08
                technical_efficiency = 0.08
                artillery = 0.08
                vehicle_engineering = 0.08
            }
            traits = {  }
        }
        # Jaroslav Heyrovsky (Skill : 6)
        CZE_Jaroslav_Heyrovsky = {
            picture = CZE_Jaroslav_Heyrovsky
            allowed = { original_tag = CZE }
            visible = {
                date > 1900.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mathematics = 0.06
                chemistry = 0.06
                management = 0.06
            }
            traits = {  }
        }
        # Charles University Prague (Skill : 5)
        CZE_Charles_University_Prague = {
            picture = CZE_Charles_University_Prague
            allowed = { original_tag = CZE }
            visible = {
                date > 1900.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.05
                chemistry = 0.05
                management = 0.05
                medicine = 0.05
            }
            traits = {  }
        }
        # Ustav jaderneho vyzkumu (Skill : 3)
        CZE_Ustav_jaderneho_vyzkumu = {
            picture = CZE_Ustav_jaderneho_vyzkumu
            allowed = { original_tag = CZE }
            visible = {
                date > 1940.1.1
                date < 2000.1.1
            }
            research_bonus = {
                nuclear_physics = 0.03
                nuclear_engineering = 0.03
                mathematics = 0.03
                electronics = 0.03
                chemistry = 0.03
            }
            traits = {  }
        }
        # Zbrojovka Brno (Skill : 6)
        CZE_Zbrojovka_Brno = {
            picture = CZE_Zbrojovka_Brno
            allowed = { original_tag = CZE }
            visible = {
                date > 1930.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.06
                general_equipment = 0.06
                training = 0.06
                artillery = 0.06
                rocketry = 0.06
            }
            traits = {  }
        }
        # C-K-D (Skill : 7)
        CZE_C_K_D = {
            picture = CZE_C_K_D
            allowed = { original_tag = CZE }
            visible = {
                date > 1900.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.07
                training = 0.07
                electronics = 0.07
                rt_user_3 = 0.07
            }
            traits = {  }
        }
        # Ceská Zbrojovka (Skill : 5)
        CZE_Ceska_Zbrojovka = {
            picture = CZE_Ceska_Zbrojovka
            allowed = { original_tag = CZE }
            visible = {
                date > 1900.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.05
                general_equipment = 0.05
            }
            traits = {  }
        }
        # Avia (Skill : 5)
        CZE_Avia = {
            picture = CZE_Avia
            allowed = { original_tag = CZE }
            visible = {
                date > 1919.1.1
                date < 2000.1.1
            }
            research_bonus = {
                aeronautics = 0.05
                electronics = 0.05
                technical_efficiency = 0.05
            }
            traits = {  }
        }
        # Letov (Skill : 4)
        CZE_Letov = {
            picture = CZE_Letov
            allowed = { original_tag = CZE }
            visible = {
                date > 1920.1.1
                date < 2000.1.1
            }
            research_bonus = {
                aeronautics = 0.04
                electronics = 0.04
                technical_efficiency = 0.04
            }
            traits = {  }
        }
        # Aero (Skill : 6)
        CZE_Aero = {
            picture = CZE_Aero
            allowed = { original_tag = CZE }
            visible = {
                date > 1930.1.1
                date < 2000.1.1
            }
            research_bonus = {
                aeronautics = 0.06
                technical_efficiency = 0.06
                chemistry = 0.06
            }
            traits = {  }
        }
        # Tatra (Skill : 5)
        CZE_Tatra = {
            picture = CZE_Tatra
            allowed = { original_tag = CZE }
            visible = {
                date > 1930.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.05
                electronics = 0.05
                technical_efficiency = 0.05
                aeronautics = 0.05
            }
            traits = {  }
        }
        # Praga (Skill : 6)
        CZE_Praga = {
            picture = CZE_Praga
            allowed = { original_tag = CZE }
            visible = {
                date > 1920.1.1
                date < 2000.1.1
            }
            research_bonus = {
                mechanics = 0.06
                aeronautics = 0.06
                chemistry = 0.06
            }
            traits = {  }
        }
        # Emil Janouska (Skill : 4)
        CZE_Emil_Janouska = {
            picture = CZE_Emil_Janouska
            allowed = { original_tag = CZE }
            visible = {
                date > 1930.1.1
                date < 1939.1.1
            }
            research_bonus = {
                bomber_tactics = 0.04
                combined_arms_focus = 0.04
                training = 0.04
                piloting = 0.04
            }
            traits = {  }
        }
        # Joseph Frantisek (Skill : 5)
        CZE_Joseph_Frantisek = {
            picture = CZE_Joseph_Frantisek
            allowed = { original_tag = CZE }
            visible = {
                date > 1930.1.1
                date < 2000.1.1
            }
            research_bonus = {
                fighter_tactics = 0.05
                piloting = 0.05
                training = 0.05
            }
            traits = {  }
        }
        # Sergej Vojcechovský (Skill : 4)
        CZE_Sergej_Vojcechovsky = {
            picture = CZE_Sergej_Vojcechovsky
            allowed = { original_tag = CZE }
            visible = {
                date > 1930.1.1
                date < 1951.1.1
            }
            research_bonus = {
                training = 0.04
                small_unit_tactics = 0.04
                combined_arms_focus = 0.04
                decentralized_execution = 0.04
            }
            traits = {  }
        }
        # Jan Syrovy (Skill : 6)
        CZE_Jan_Syrovy = {
            picture = CZE_Jan_Syrovy
            allowed = { original_tag = CZE }
            visible = {
                date > 1920.1.1
                date < 2000.1.1
            }
            research_bonus = {
                centralized_execution = 0.06
                large_unit_tactics = 0.06
                individual_courage = 0.06
                infantry_focus = 0.06
                training = 0.06
            }
            traits = {  }
        }
        # Ludvik Svoboda (Skill : 5)
        CZE_Ludvik_Svoboda = {
            picture = CZE_Ludvik_Svoboda
            allowed = { original_tag = CZE }
            visible = {
                date > 1945.1.1
                date < 2000.1.1
            }
            research_bonus = {
                small_unit_tactics = 0.05
                decentralized_execution = 0.05
                combined_arms_focus = 0.05
                individual_courage = 0.05
            }
            traits = {  }
        }
    }
}