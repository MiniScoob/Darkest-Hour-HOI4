ideas = {

	theorist = {
		HAI_emilie_lescot = {

			picture = generic_army_african_4
			
			allowed = {
				original_tag = HAI
			}
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}
		
		HAI_pascale_b_g_guillaume = { 

			picture = generic_air_african_2
			
			allowed = {
				original_tag = HAI
			}
			
			research_bonus = {
				air_doctrine = 0.07
			}
			
			traits = { air_warfare_theorist }
		}

		HAI_georges_leger = {
					
					
			allowed = {
				original_tag = HAI
			}
			
			picture = generic_political_advisor_african_3
			
			research_bonus = {
				naval_doctrine = 0.07
			}
			
			traits = { naval_theorist }
		}
	}

	high_command = {

		HAI_ernst_severe = {

			picture = generic_army_african_2
			
			allowed = {
				original_tag = HAI
			}
			
			traits = { army_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		HAI_paul_magloire = {

			picture = generic_army_african_5
			
			allowed = {
				original_tag = HAI
			}
			
			traits = { army_concealment_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		HAI_andre_dumont = {

			picture = generic_army_african_3
			
			allowed = {
				original_tag = HAI
			}
			
			traits = { army_cavalry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		HAI_belmeau_max_pierre = {

			picture = generic_air_african_2
			
			allowed = {
				original_tag = HAI
			}
			
			traits = { air_airborne_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}
}