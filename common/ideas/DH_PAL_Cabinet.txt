ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Yussef Pasha al-Khalidi
		PAL_HoG_Yussef_Pasha_alKhalidi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Yussef_Pasha_alKhalidi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Political_Protege }
		}
	# Sir Herbert Plumer
		PAL_HoG_Sir_Herbert_Plumer = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Herbert_Plumer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# Sir John R. Chancellor
		PAL_HoG_Sir_John_R_Chancellor = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_John_R_Chancellor_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Ahmad Shukairy
		PAL_HoG_Ahmad_Shukairy = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_Shukairy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Haj Mustafa Abboushi
		PAL_HoG_Haj_Mustafa_Abboushi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Haj_Mustafa_Abboushi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Ambitious_Union_Boss }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Ragheed Nashasbibi
		PAL_FM_Ragheed_Nashasbibi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Ragheed_Nashasbibi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Iron_Fisted_Brute }
		}
	# Sir Greenfell Wauchope
		PAL_FM_Sir_Greenfell_Wauchope = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Greenfell_Wauchope_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Nazif al-Khalidi
		PAL_FM_Nazif_alKhalidi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Nazif_alKhalidi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Harry C. Luke
		PAL_FM_Harry_C_Luke = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Harry_C_Luke_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Hafiz Pasha Abdul Hadi
		PAL_FM_Hafiz_Pasha_Abdul_Hadi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Hafiz_Pasha_Abdul_Hadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Apologetic_Clerk }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Awni Abdul Hadi
		PAL_MoS_Awni_Abdul_Hadi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Awni_Abdul_Hadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Crooked_Kleptocrat }
		}
	# George Antinous
		PAL_MoS_George_Antinous = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = George_Antinous_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Musa Qassem al-Husseini
		PAL_MoS_Musa_Qassem_alHusseini = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Musa_Qassem_alHusseini_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Nicolas Abdo
		PAL_MoS_Nicolas_Abdo = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Nicolas_Abdo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Dekel Shukri
		PAL_MoS_Dekel_Shukri = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Dekel_Shukri_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Back_Stabber }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Alfred Roch
		PAL_AM_Alfred_Roch = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Alfred_Roch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Air_To_Ground_Proponent }
		}
	# Abdullah Jaffer Dalal
		PAL_AM_Abdullah_Jaffer_Dalal = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Abdullah_Jaffer_Dalal_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_Superiority_Proponent }
		}
	# George Humsi
		PAL_AM_George_Humsi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = George_Humsi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Ruhi Abdul Hadi
		PAL_AM_Ruhi_Abdul_Hadi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Ruhi_Abdul_Hadi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Sharif Nasir al-Qagli
		PAL_AM_Sharif_Nasir_alQagli = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Sharif_Nasir_alQagli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Infantry_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Dr. Abu So'oud
		PAL_HoI_Dr_Abu_Sooud = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Dr_Abu_Sooud_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Technical_Specialist }
		}
	# Massoud al-Jinnah
		PAL_HoI_Massoud_alJinnah = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Massoud_alJinnah_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Industrial_Specialist }
		}
	# Frederic G.Peake
		PAL_HoI_Frederic_GPeake = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Frederic_GPeake_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Faruk Haydar
		PAL_HoI_Faruk_Haydar = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Faruk_Haydar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
	# Majid Abd-el-Haqq
		PAL_HoI_Majid_AbdelHaqq = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Majid_AbdelHaqq_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Dismal_Enigma }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Ali Djemal Pasha
		PAL_CoStaff_Ali_Djemal_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Djemal_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Fire_Support }
		}
	# Sir Herbert Plumer
		PAL_CoStaff_Sir_Herbert_Plumer = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Herbert_Plumer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Sir Herbert Samuel
		PAL_CoStaff_Sir_Herbert_Samuel = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Herbert_Samuel_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Alan G. Cunningham
		PAL_CoStaff_Alan_G_Cunningham = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Alan_G_Cunningham_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Hasan Salama
		PAL_CoStaff_Hasan_Salama = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Hasan_Salama_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Faruk Haydar
		PAL_CoStaff_Faruk_Haydar = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Faruk_Haydar_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Jaafra Pasha
		PAL_CoStaff_Jaafra_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Jaafra_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Defence }
		}
	# Yasser Arafat
		PAL_CoStaff_Yasser_Arafat = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1961.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Yasser_Arafat_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Mass_Combat }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Aly al-Muttalib
		PAL_CoArmy_Aly_alMuttalib = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Aly_alMuttalib_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Guns_And_Butter_Doctrine }
		}
	# Sir Greenfell Wauchope
		PAL_CoArmy_Sir_Greenfell_Wauchope = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Greenfell_Wauchope_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# G.H.C. Spinls
		PAL_CoArmy_GHC_Spinls = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = GHC_Spinls_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Lord John Gort
		PAL_CoArmy_Lord_John_Gort = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Lord_John_Gort_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Liwa al-Mawawiyi
		PAL_CoArmy_Liwa_alMawawiyi = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Liwa_alMawawiyi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Sharif Nasir al-Qagli
		PAL_CoArmy_Sharif_Nasir_alQagli = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Sharif_Nasir_alQagli_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Ali Djemal Pasha
		PAL_CoNavy_Ali_Djemal_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Ali_Djemal_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Base_Control_Doctrine }
		}
	# Rodney Tatton-Sykes
		PAL_CoNavy_Rodney_TattonSykes = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Rodney_TattonSykes_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# F.B. Tours
		PAL_CoNavy_FB_Tours = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = FB_Tours_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Alfred Howard
		PAL_CoNavy_Alfred_Howard = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Alfred_Howard_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Ahmad Wasi Pasha
		PAL_CoNavy_Ahmad_Wasi_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Ahmad_Wasi_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Power_Projection_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Mohammed Azmy Pasha
		PAL_CoAir_Mohammed_Azmy_Pasha = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				NS_Minister_Allowed = yes
				NOT = { has_country_flag = Mohammed_Azmy_Pasha_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Army_Aviation_Doctrine }
		}
	# Abdullah Jaffer Dalal
		PAL_CoAir_Abdullah_Jaffer_Dalal = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Abdullah_Jaffer_Dalal_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Air_Superiority_Doctrine }
		}
	# J.T. Fullhard
		PAL_CoAir_JT_Fullhard = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = JT_Fullhard_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# James Rogers
		PAL_CoAir_James_Rogers = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = James_Rogers_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Muhammed al-Taif
		PAL_CoAir_Muhammed_alTaif = {
			picture = Generic_Portrait
			allowed = { tag = PAL }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Muhammed_alTaif_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Naval_Aviation_Doctrine }
		}
	}
}
