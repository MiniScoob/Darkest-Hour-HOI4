ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Blazo Djukanovic
		MTN_HoG_Blazo_Djukanovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Blazo_Djukanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Ambitious_Union_Boss }
		}
	# Sekula Drljevic
		MTN_HoG_Sekula_Drljevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1941.1.1
				date < 1944.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Sekula_Drljevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Ambitious_Union_Boss }
		}
	# Blazo Djukanovic
		MTN_HoG_Blazo_Djukanovic_2 = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1941.1.1
				date < 1944.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Blazo_Djukanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Ambitious_Union_Boss }
		}
	# Ljubo Vukcevic
		MTN_HoG_Ljubo_Vukcevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1943.1.1
				date < 1963.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Ljubo_Vukcevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Ambitious_Union_Boss }
		}
	# Janko Vukotic
		MTN_HoG_Janko_Vukotic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Janko_Vukotic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Mitar Martinovic
		MTN_HoG_Mitar_Martinovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Mitar_Martinovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
	# Milo Matanovic
		MTN_HoG_Milo_Matanovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Milo_Matanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Lazar Mijuskovic
		MTN_HoG_Lazar_Mijuskovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Lazar_Mijuskovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Andrija Radovic
		MTN_HoG_Andrija_Radovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Andrija_Radovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Evgenije Popovic
		MTN_HoG_Evgenije_Popovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Evgenije_Popovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Savo Cerovic
		MTN_HoG_Savo_Cerovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Savo_Cerovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Filip Vujanovic
		MTN_HoG_Filip_Vujanovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Filip_Vujanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Milo Djukanovic
		MTN_HoG_Milo_Djukanovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1991.1.1
				date < 2020.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Milo_Djukanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Pavle Djurisic
		MTN_HoG_Pavle_Djurisic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Pavle_Djurisic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Old_General }
		}
	# Dordije Pajkovic
		MTN_HoG_Dordije_Pajkovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1962.1.1
				date < 1970.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Dordije_Pajkovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Veselin Duranovic
		MTN_HoG_Veselin_Duranovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1963.1.1
				date < 1964.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Veselin_Duranovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Ambitious_Union_Boss }
		}
	# Vidoje Zarkovic
		MTN_HoG_Vidoje_Zarkovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1967.1.1
				date < 2001.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Vidoje_Zarkovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Corporate_Suit }
		}
	# Zarko Bulajic
		MTN_HoG_Zarko_Bulajic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1969.1.1
				date < 2010.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Zarko_Bulajic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Marko Orlandic
		MTN_HoG_Marko_Orlandic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1974.1.1
				date < 2020.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Marko_Orlandic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_Airmarshal }
		}
	# Momcilo Cemovic
		MTN_HoG_Momcilo_Cemovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1978.1.1
				date < 2002.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Momcilo_Cemovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Corporate_Suit }
		}
	# Radivoje Brajovic
		MTN_HoG_Radivoje_Brajovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1982.1.1
				date < 2020.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Radivoje_Brajovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
	# Miodrag Vlahovic
		MTN_HoG_Miodrag_Vlahovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1984.1.1
				date < 1995.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Miodrag_Vlahovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Vuko Vukadinovic
		MTN_HoG_Vuko_Vukadinovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1986.1.1
				date < 1994.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Vuko_Vukadinovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Smiling_Oilman }
		}
	# Radoje Kontic
		MTN_HoG_Radoje_Kontic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1989.1.1
				date < 2020.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Radoje_Kontic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Milovan Dijas
		MTN_HoG_Milovan_Dijas = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				ST_Minister_Allowed = yes
				NOT = { has_country_flag = Milovan_Dijas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
	# Nikola Miljanic
		MTN_HoG_Nikola_Miljanic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				ST_Minister_Allowed = yes
				NOT = { has_country_flag = Nikola_Miljanic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Smiling_Oilman }
		}
	# Milos Rasovic
		MTN_HoG_Milos_Rasovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				ST_Minister_Allowed = yes
				NOT = { has_country_flag = Milos_Rasovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
	# Nikola Kovacevic
		MTN_HoG_Nikola_Kovacevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				ST_Minister_Allowed = yes
				NOT = { has_country_flag = Nikola_Kovacevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Political_Protege }
		}
	# Filip Bajkovic
		MTN_HoG_Filip_Bajkovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				ST_Minister_Allowed = yes
				NOT = { has_country_flag = Filip_Bajkovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Smiling_Oilman }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Jovan S. Plamenec
		MTN_FM_Jovan_S_Plamenec = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Jovan_S_Plamenec_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_Iron_Fisted_Brute }
		}
	# Mitar Martinovic
		MTN_FM_Mitar_Martinovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Mitar_Martinovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F FM_General_Staffer }
		}
	# Petar Plamenac
		MTN_FM_Petar_Plamenac = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Petar_Plamenac_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Mirko Mijuskovic
		MTN_FM_Mirko_Mijuskovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Mirko_Mijuskovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Lazar Mijuskovic
		MTN_FM_Lazar_Mijuskovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Lazar_Mijuskovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# Milutin Tomanovic
		MTN_FM_Milutin_Tomanovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Milutin_Tomanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# Milutin Vucinec
		MTN_FM_Milutin_Vucinec = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Milutin_Vucinec_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Jovan Ducic
		MTN_FM_Jovan_Ducic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Jovan_Ducic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Ljubo Vukcevic
		MTN_MoS_Ljubo_Vukcevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Ljubo_Vukcevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Silent_Lawyer }
		}
	# Karl Wolff
		MTN_MoS_Karl_Wolff = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Wolff_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F MoS_Efficient_Sociopath }
		}
	# Labud Gojnic
		MTN_MoS_Labud_Gojnic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Labud_Gojnic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Jovan Plamenac
		MTN_MoS_Jovan_Plamenac = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Jovan_Plamenac_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Savo Vuletic
		MTN_MoS_Savo_Vuletic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Savo_Vuletic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Janko Spasojevic
		MTN_MoS_Janko_Spasojevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Janko_Spasojevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Mitrofan Ban
		MTN_MoS_Mitrofan_Ban = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Mitrofan_Ban_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Ivan Gvenkovski
		MTN_MoS_Ivan_Gvenkovski = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Ivan_Gvenkovski_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Eugen von Albori
		MTN_AM_Eugen_von_Albori = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Eugen_von_Albori_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Corrupt_Kleptocrat }
		}
	# Krste Djuricic
		MTN_AM_Krste_Djuricic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Krste_Djuricic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F AM_Tank_Proponent }
		}
	# Risto Popovic
		MTN_AM_Risto_Popovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Risto_Popovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Military_Entrepreneur }
		}
	# Andrija Radovic
		MTN_AM_Andrija_Radovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Andrija_Radovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Administrative_Genius }
		}
	# Stanisa Ilic
		MTN_AM_Stanisa_Ilic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Stanisa_Ilic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Resource_Industrialist }
		}
	# Duhan Vukovitch
		MTN_AM_Duhan_Vukovitch = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Duhan_Vukovitch_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_To_Sea_Proponent }
		}
	# Goce Kolicevski
		MTN_AM_Goce_Kolicevski = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Goce_Kolicevski_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Infantry_Proponent }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Milutin Bojic
		MTN_HoI_Milutin_Bojic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Milutin_Bojic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoI_Political_Specialist }
		}
	# Ljubomir Brkic
		MTN_HoI_Ljubomir_Brkic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Ljubomir_Brkic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Marko Radulovic
		MTN_HoI_Marko_Radulovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1915.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Marko_Radulovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Janko Spasojevic
		MTN_HoI_Janko_Spasojevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1916.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Janko_Spasojevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Mirko Tirkuvic
		MTN_HoI_Mirko_Tirkuvic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Mirko_Tirkuvic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Kiro Filipovic
		MTN_HoI_Kiro_Filipovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Kiro_Filipovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Luka Gojna
		MTN_CoStaff_Luka_Gojna = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Luka_Gojna_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoStaff_School_Of_Psychology }
		}
	# Bozidar Jankovic
		MTN_CoStaff_Bozidar_Jankovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Bozidar_Jankovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Janko Vukotic
		MTN_CoStaff_Janko_Vukotic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Janko_Vukotic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Psychology }
		}
	# Nikola I
		MTN_CoStaff_Nikola_I = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Nikola_I_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Baltasar Uljarevic
		MTN_CoStaff_Baltasar_Uljarevic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Baltasar_Uljarevic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Pavle Djurisic
		MTN_CoStaff_Pavle_Djurisic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Pavle_Djurisic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Defence }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Anto Gvozdenovic
		MTN_CoArmy_Anto_Gvozdenovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Anto_Gvozdenovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Wilhelm Behrendt
		MTN_CoArmy_Wilhelm_Behrendt = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Wilhelm_Behrendt_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoArmy_Static_Defence_Doctrine }
		}
	# Janko Vukotic
		MTN_CoArmy_Janko_Vukotic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Janko_Vukotic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Masan Bozovic
		MTN_CoArmy_Masan_Bozovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Masan_Bozovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Decisive_Battle_Doctrine }
		}
	# Radomir Vesovic
		MTN_CoArmy_Radomir_Vesovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Radomir_Vesovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Elastic_Defence_Doctrine }
		}
	# Djuro Dragov Soc
		MTN_CoArmy_Djuro_Dragov_Soc = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Djuro_Dragov_Soc_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Nikola I
		MTN_CoArmy_Nikola_I = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Nikola_I_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Bozidar Jankovic
		MTN_CoArmy_Bozidar_Jankovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Bozidar_Jankovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Guns_And_Butter_Doctrine }
		}
	# Milan Cheric
		MTN_CoArmy_Milan_Cheric = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Milan_Cheric_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Elastic_Defence_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Mitar Martinovic
		MTN_CoNavy_Mitar_Martinovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Mitar_Martinovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoNavy_Open_Seas_Doctrine }
		}
	# Evgenij Popovic
		MTN_CoNavy_Evgenij_Popovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Evgenij_Popovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Djuradj Negros
		MTN_CoNavy_Djuradj_Negros = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Djuradj_Negros_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Bora Todorovic
		MTN_CoAir_Bora_Todorovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Bora_Todorovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F CoAir_Naval_Aviation_Doctrine }
		}
	# Branko Petrovic
		MTN_CoAir_Branko_Petrovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Branko_Petrovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Slobodan Jovanovic
		MTN_CoAir_Slobodan_Jovanovic = {
			picture = Generic_Portrait
			allowed = { tag = MTN }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Slobodan_Jovanovic_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
