ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Lord Josslyn Erroll
		EAF_HoG_Lord_Josslyn_Erroll = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Lord_Josslyn_Erroll_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# Sir John Hathorn Hall
		EAF_HoG_Sir_John_Hathorn_Hall = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_John_Hathorn_Hall_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Jomo Kenyatta
		EAF_HoG_Jomo_Kenyatta = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Jomo_Kenyatta_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Apolo Milton Obote
		EAF_HoG_Apolo_Milton_Obote = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Apolo_Milton_Obote_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Happy_Amateur }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Pio Gama Pinto
		EAF_FM_Pio_Gama_Pinto = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Pio_Gama_Pinto_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Robert Brooke-Popham
		EAF_FM_Robert_BrookePopham = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_BrookePopham_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_General_Staffer }
		}
	# Henry G. Pilling
		EAF_FM_Henry_G_Pilling = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_G_Pilling_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Keterne Yifru
		EAF_FM_Keterne_Yifru = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Keterne_Yifru_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Biased_Intellectual }
		}
	# Joseph Murumbi
		EAF_FM_Joseph_Murumbi = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Murumbi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_The_Cloak_N_Dagger_Schemer }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Makhan Singh
		EAF_MoS_Makhan_Singh = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Makhan_Singh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crooked_Kleptocrat }
		}
	# Ide DeLisle
		EAF_MoS_Ide_DeLisle = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Ide_DeLisle_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Back_Stabber }
		}
	# Sir Harold McMichael
		EAF_MoS_Sir_Harold_McMichael = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Harold_McMichael_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Vincent Goncalves Glenday
		EAF_MoS_Vincent_Goncalves_Glenday = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Vincent_Goncalves_Glenday_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Benedicto Kiwanuka
		EAF_MoS_Benedicto_Kiwanuka = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Benedicto_Kiwanuka_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Dedan Kimathi
		EAF_MoS_Dedan_Kimathi = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Dedan_Kimathi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Alexius de Laszlo
		EAF_AM_Alexius_de_Laszlo = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Alexius_de_Laszlo_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Infantry_Proponent }
		}
	# Chandrakanth Mehta
		EAF_AM_Chandrakanth_Mehta = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Chandrakanth_Mehta_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Laissez-faire_Capitalist }
		}
	# Henry Monck-Mason Moore
		EAF_AM_Henry_MonckMason_Moore = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_MonckMason_Moore_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Battle_Fleet_Proponent }
		}
	# Charles Farquharson Dundas
		EAF_AM_Charles_Farquharson_Dundas = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1940.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Farquharson_Dundas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Air_Superiority_Proponent }
		}
	# Ronald Ngala
		EAF_AM_Ronald_Ngala = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Ronald_Ngala_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Nasirdas Mehta
		EAF_AM_Nasirdas_Mehta = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Nasirdas_Mehta_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Dravi Kanaphathuya
		EAF_AM_Dravi_Kanaphathuya = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Dravi_Kanaphathuya_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S AM_Theoretical_Scientist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Charles Witherspoon
		EAF_HoI_Charles_Witherspoon = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Witherspoon_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Mark Young
		EAF_HoI_Mark_Young = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Mark_Young_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Tarcisius Ngagalekumtwa
		EAF_HoI_Tarcisius_Ngagalekumtwa = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Tarcisius_Ngagalekumtwa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Naval_Intelligence_Specialist }
		}
	# Ernest Hemingway
		EAF_HoI_Ernest_Hemingway = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Ernest_Hemingway_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Rashid Kawawa
		EAF_HoI_Rashid_Kawawa = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Rashid_Kawawa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoI_Technical_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Richard Carver
		EAF_CoStaff_Richard_Carver = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Richard_Carver_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Manoeuvre }
		}
	# Henry Dundas Stevenson
		EAF_CoStaff_Henry_Dundas_Stevenson = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_Dundas_Stevenson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Jeremy J. Pierson
		EAF_CoStaff_Jeremy_J_Pierson = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Jeremy_J_Pierson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Edward Mutesa
		EAF_CoStaff_Edward_Mutesa = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Mutesa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Mass_Combat }
		}
	# Henry Jumara
		EAF_CoStaff_Henry_Jumara = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_Jumara_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Fire_Support }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Peter Mills
		EAF_CoArmy_Peter_Mills = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Peter_Mills_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Robert Brooke-Popham
		EAF_CoArmy_Robert_BrookePopham = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_BrookePopham_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Henry G. Pilling
		EAF_CoArmy_Henry_G_Pilling = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_G_Pilling_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Ongeri Bogita
		EAF_CoArmy_Ongeri_Bogita = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Ongeri_Bogita_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Juma Datawi
		EAF_CoArmy_Juma_Datawi = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Juma_Datawi_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Elastic_Defence_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Daudi Chawa
		EAF_CoNavy_Daudi_Chawa = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Daudi_Chawa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Indirect_Approach_Doctrine }
		}
	# William Denis Battershill
		EAF_CoNavy_William_Denis_Battershill = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = William_Denis_Battershill_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Nashruddin Mehta
		EAF_CoNavy_Nashruddin_Mehta = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Nashruddin_Mehta_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Edward Montford
		EAF_CoNavy_Edward_Montford = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Montford_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Makhan Singh
		EAF_CoAir_Makhan_Singh = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Makhan_Singh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Naval_Aviation_Doctrine }
		}
	# Charles Farquharson Dundas
		EAF_CoAir_Charles_Farquharson_Dundas = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Farquharson_Dundas_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# James Howe-Dobson
		EAF_CoAir_James_HoweDobson = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = James_HoweDobson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Nasirdas Mehta
		EAF_CoAir_Nasirdas_Mehta = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Nasirdas_Mehta_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# Edward Montford
		EAF_CoAir_Edward_Montford = {
			picture = Generic_Portrait
			allowed = { tag = EAF }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Edward_Montford_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
