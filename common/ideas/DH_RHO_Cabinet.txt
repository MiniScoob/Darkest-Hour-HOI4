ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Ian Smith
		RHO_HoG_Ian_Smith = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1964.1.1
				date < 2008.1.1
				FA_Minister_Allowed = yes
				NOT = { has_country_flag = Ian_Smith_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_F HoG_Naive_Optimist }
		}
	# Donald McKenzie-Kennedy
		RHO_HoG_Donald_McKenzieKennedy = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Donald_McKenzieKennedy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Airmarshal }
		}
	# Edgar Whitehead
		RHO_HoG_Edgar_Whitehead = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1958.1.1
				date < 1972.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Edgar_Whitehead_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Political_Protege }
		}
	# Winston Field
		RHO_HoG_Winston_Field = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1962.1.1
				date < 1970.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Winston_Field_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Godfrey Martin Huggins
		RHO_HoG_Godfrey_Martin_Huggins = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Godfrey_Martin_Huggins_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Sir Roy Welensky
		RHO_HoG_Sir_Roy_Welensky = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Roy_Welensky_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Sir Charles Arden-Clarke
		RHO_HoG_Sir_Charles_ArdenClarke = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Charles_ArdenClarke_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Old_General }
		}
	# Hastings Kamuzu Banda
		RHO_HoG_Hastings_Kamuzu_Banda = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Hastings_Kamuzu_Banda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Robert Mugabe
		RHO_HoG_Robert_Mugabe = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1980.1.1
				date < 2020.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_Mugabe_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Backroom_Backstabber }
		}
	# Simon Vengai Muzenda
		RHO_HoG_Simon_Vengai_Muzenda = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1987.1.1
				date < 2004.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Simon_Vengai_Muzenda_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Old_General }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Kgoshi III Khama
		RHO_FM_Kgoshi_III_Khama = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Kgoshi_III_Khama_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Biased_Intellectual }
		}
	# Sir Evelyn Baring
		RHO_FM_Sir_Evelyn_Baring = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Evelyn_Baring_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Iron_Fisted_Brute }
		}
	# Sir Harold B. Kittermaster
		RHO_FM_Sir_Harold_B_Kittermaster = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Harold_B_Kittermaster_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Sir Seretse Khama
		RHO_FM_Sir_Seretse_Khama = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Seretse_Khama_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Enderby Khamatse
		RHO_MoS_Enderby_Khamatse = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Enderby_Khamatse_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
	# Maxwell MacDiarmid
		RHO_MoS_Maxwell_MacDiarmid = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Maxwell_MacDiarmid_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Man_Of_The_People }
		}
	# Anthony Sillery
		RHO_MoS_Anthony_Sillery = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Anthony_Sillery_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Robert Smythe-Cramer
		RHO_MoS_Robert_SmytheCramer = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_SmytheCramer_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Crooked_Kleptocrat }
		}
	# Violet Grigg
		RHO_MoS_Violet_Grigg = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Violet_Grigg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Silent_Lawyer }
		}
	# Henry Nkumbula
		RHO_MoS_Henry_Nkumbula = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Henry_Nkumbula_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Muzi Hlongwa
		RHO_MoS_Muzi_Hlongwa = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Muzi_Hlongwa_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Sir Roy Welensky
		RHO_AM_Sir_Roy_Welensky = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Roy_Welensky_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Air_To_Ground_Proponent }
		}
	# Sir Robert J. Hudson
		RHO_AM_Sir_Robert_J_Hudson = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Robert_J_Hudson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Administrative_Genius }
		}
	# Mulena Yomohulu
		RHO_AM_Mulena_Yomohulu = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Mulena_Yomohulu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Corrupt_Kleptocrat }
		}
	# Paed O'Reilly
		RHO_AM_Paed_OReilly = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Paed_OReilly_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Eubule James Waddington
		RHO_AM_Eubule_James_Waddington = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Eubule_James_Waddington_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Theoretical_Scientist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Peter Youens
		RHO_HoI_Peter_Youens = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Peter_Youens_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Naval_Intelligence_Specialist }
		}
	# John Alexander Maybin
		RHO_HoI_John_Alexander_Maybin = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = John_Alexander_Maybin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Sir Philip W. Richardson
		RHO_HoI_Sir_Philip_W_Richardson = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Philip_W_Richardson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Duncan McAllister
		RHO_HoI_Duncan_McAllister = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Duncan_McAllister_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# John Mwanakatwe
		RHO_HoI_John_Mwanakatwe = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = John_Mwanakatwe_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Technical_Specialist }
		}
	# Tshekedi Khama
		RHO_HoI_Tshekedi_Khama = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Tshekedi_Khama_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoI_Logistics_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# G.W.D. Nicholson
		RHO_CoStaff_GWD_Nicholson = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = GWD_Nicholson_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Mass_Combat }
		}
	# Eric de Burgh
		RHO_CoStaff_Eric_de_Burgh = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Eric_de_Burgh_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Thomas Rees
		RHO_CoStaff_Thomas_Rees = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Thomas_Rees_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Mulena Yomohulu
		RHO_CoStaff_Mulena_Yomohulu = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Mulena_Yomohulu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# C.M. Grigg
		RHO_CoStaff_CM_Grigg = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = CM_Grigg_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Ferdinand Charles Stanley
		RHO_CoArmy_Ferdinand_Charles_Stanley = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Ferdinand_Charles_Stanley_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Philip Carrington
		RHO_CoArmy_Philip_Carrington = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Philip_Carrington_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Dennis C. Capell-Dunn
		RHO_CoArmy_Dennis_C_CapellDunn = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Dennis_C_CapellDunn_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Mbumu wa Ltunga
		RHO_CoArmy_Mbumu_wa_Ltunga = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Mbumu_wa_Ltunga_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Elastic_Defence_Doctrine }
		}
	# Yomohulu Mburnu
		RHO_CoArmy_Yomohulu_Mburnu = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Yomohulu_Mburnu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# H.H. Litunga Yeta III
		RHO_CoNavy_HH_Litunga_Yeta_III = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = HH_Litunga_Yeta_III_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
	# Cecil Drummond
		RHO_CoNavy_Cecil_Drummond = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Cecil_Drummond_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Power_Projection_Doctrine }
		}
	# Tommy Gould
		RHO_CoNavy_Tommy_Gould = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Tommy_Gould_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# George Squance
		RHO_CoNavy_George_Squance = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = George_Squance_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Mbumu wa Ltunga
		RHO_CoNavy_Mbumu_wa_Ltunga = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = Mbumu_wa_Ltunga_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Donald McKenzie-Kennedy
		RHO_CoAir_Donald_McKenzieKennedy = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Donald_McKenzieKennedy_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Sir Charles Arden-Clarke
		RHO_CoAir_Sir_Charles_ArdenClarke = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				ML_Minister_Allowed = yes
				NOT = { has_country_flag = Sir_Charles_ArdenClarke_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	# Herbert Croucher
		RHO_CoAir_Herbert_Croucher = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Herbert_Croucher_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
	# John Mwanakatwe
		RHO_CoAir_John_Mwanakatwe = {
			picture = Generic_Portrait
			allowed = { tag = RHO }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SD_Minister_Allowed = yes
				NOT = { has_country_flag = John_Mwanakatwe_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
