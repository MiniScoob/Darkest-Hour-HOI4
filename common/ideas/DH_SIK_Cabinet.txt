ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Isa Yusuf Alptekin
		SIK_HoG_Isa_Yusuf_Alptekin = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1940.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Isa_Yusuf_Alptekin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Xoja Niyaz Haji
		SIK_HoG_Xoja_Niyaz_Haji = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1912.1.1
				date < 1942.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Xoja_Niyaz_Haji_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Naive_Optimist }
		}
	# Sheng Shicai
		SIK_HoG_Sheng_Shicai = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S HoG_Old_General }
		}
	# Ehmetjan Qasim
		SIK_HoG_Ehmetjan_Qasim = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Ehmetjan_Qasim_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Sheng Shicai
		SIK_FM_Sheng_Shicai = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Isa Yusuf Alptekin
		SIK_FM_Isa_Yusuf_Alptekin = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1940.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Isa_Yusuf_Alptekin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Sheng Shicai
		SIK_FM_Sheng_Shicai_2 = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S FM_Great_Compromiser }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Zhang Xiliang
		SIK_MoS_Zhang_Xiliang = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Xiliang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crooked_Kleptocrat }
		}
	# Ishakjan Monhakiyev
		SIK_MoS_Ishakjan_Monhakiyev = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Ishakjan_Monhakiyev_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Kunimu Khan
		SIK_MoS_Kunimu_Khan = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Kunimu_Khan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Habibullah Huseinov
		SIK_AM_Habibullah_Huseinov = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Habibullah_Huseinov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Infantry_Proponent }
		}
	# A.V. Fedyunskij
		SIK_AM_AV_Fedyunskij = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = AV_Fedyunskij_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Infantry_Proponent }
		}
	# Isa Yusuf Alptekin
		SIK_AM_Isa_Yusuf_Alptekin = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1940.1.1
				SC_Minister_Allowed = yes
				NOT = { has_country_flag = Isa_Yusuf_Alptekin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# Kunimu Khan
		SIK_AM_Kunimu_Khan = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				SL_Minister_Allowed = yes
				NOT = { has_country_flag = Kunimu_Khan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_D AM_Military_Entrepreneur }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Viktor A.F. Polinov
		SIK_HoI_Viktor_AF_Polinov = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Viktor_AF_Polinov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Industrial_Specialist }
		}
	# Zhang Xiliang
		SIK_HoI_Zhang_Xiliang = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Xiliang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Dismal_Enigma }
		}
	# Dalerim Khan
		SIK_HoI_Dalerim_Khan = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Dalerim_Khan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Liu Ben
		SIK_HoI_Liu_Ben = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Liu_Ben_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Technical_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Sheng Shicai
		SIK_CoStaff_Sheng_Shicai = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Psychology }
		}
	# Yol Bars
		SIK_CoStaff_Yol_Bars = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Yol_Bars_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Mass_Combat }
		}
	# Sheng Shicai
		SIK_CoStaff_Sheng_Shicai_2 = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Sheng Shicai
		SIK_CoArmy_Sheng_Shicai = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Yol Bars
		SIK_CoArmy_Yol_Bars = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Yol_Bars_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Elastic_Defence_Doctrine }
		}
	# Sheng Shicai
		SIK_CoArmy_Sheng_Shicai_2 = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LWR_Minister_Allowed = yes
				NOT = { has_country_flag = Sheng_Shicai_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_S CoArmy_Guns_And_Butter_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Sa Zhenbing
		SIK_CoNavy_Sa_Zhenbing = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Sa_Zhenbing_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Indirect_Approach_Doctrine }
		}
	# Sa Zhenbing
		SIK_CoNavy_Sa_Zhenbing_2 = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Sa_Zhenbing_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Piotr Orlov
		SIK_CoAir_Piotr_Orlov = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				PA_Minister_Allowed = yes
				NOT = { has_country_flag = Piotr_Orlov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Piotr Orlov
		SIK_CoAir_Piotr_Orlov_2 = {
			picture = Generic_Portrait
			allowed = { tag = SIK }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				LE_Minister_Allowed = yes
				NOT = { has_country_flag = Piotr_Orlov_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_C CoAir_Army_Aviation_Doctrine }
		}
	}
}
