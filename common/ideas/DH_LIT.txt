ideas = {

	theorist = {

		LIT_jonas_cernius = {
					
			picture = generic_army_europe_4

			allowed = {
				original_tag = LIT
			}
			
			research_bonus = {
				land_doctrine = 0.07
			}
			
			traits = { military_theorist }
		}

		LIT_vincentas_latozaite = {
					
			picture = generic_air_europe_1
				
			allowed = {
				original_tag = LIT
			}
			
			research_bonus = {
				air_doctrine = 0.07
			}
			
			traits = { air_warfare_theorist }
		}
	}

	high_command = {

		LIT_juoza_barzda_bradauskas = {

			picture = generic_army_europe_1
			
			allowed = {
				original_tag = LIT
			}
			
			traits = { army_artillery_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		LIT_albinas_cepas = {

			picture = generic_army_europe_3
			
			allowed = {
				original_tag = LIT
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		LIT_kazys_musteikis = {

			picture = generic_army_europe_5
			
			allowed = {
				original_tag = LIT
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		LIT_adolfas_ramanauskas = {

			picture = generic_army_europe_1
			
			allowed = {
				original_tag = LIT
			}
			
			traits = { army_concealment_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = { 		
			
		designer = yes
		
		anbo = {
			
			picture = generic_air_manufacturer_3

			
			allowed = {
				original_tag = LIT
			}
			
			research_bonus = {
				air_equipment = 0.10
			}
			
			traits = { light_aircraft_manufacturer }
			
			equipment_bonus = {
				# made very few training planes, bombers
			}
			
			ai_will_do = {
				factor = 1
			}
		}
	}
}