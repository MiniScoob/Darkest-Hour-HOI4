defined_text = {
	name = GetFocusEventTitle
	
	text = {
		trigger = {
			check_variable = { v = 1 }
		}
		localization_key = FOCUS_EVENT_TITLE_1
	}
}

defined_text = {
	name = GetFocusEventDesc
	
	text = {
		trigger = {
			check_variable = { v = 1 }
		}
		localization_key = FOCUS_EVENT_DESC_1
	}
}

defined_text = {
	name = GetFocusEventButtonText
	
	text = {
		trigger = {
			check_variable = { v = 1 }
		}
		localization_key = FOCUS_EVENT_BUTTON_1
	}
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = FOCUS_EVENT_BUTTON_GENERIC
	}
}

defined_text = {
	name = GetFocusEventPicture
	
	text = {
		trigger = {
			check_variable = { v = 1 }
		}
		localization_key = "GFX_focus_event_picture_test"
	}
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = "GFX_focus_event_picture_test"
	}
}