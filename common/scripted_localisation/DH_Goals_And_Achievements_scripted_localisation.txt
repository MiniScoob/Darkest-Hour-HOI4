defined_text = {
	name = GetGoalOrAchievementName
	
	text = {
		trigger = {
			always = yes
			set_temp_variable = { goal_or_achievement_id = v }
		}
		localization_key = "[This.GetGoalOrAchievementNameOtherContext]"
	}
}

defined_text = {
	name = GetGoalOrAchievementNameOtherContext
	
	text = {
		trigger = {
			check_variable = { goal_or_achievement_id = 1 }
		}
		localization_key = GOAL_OR_ACHIEVEMENT_1
	}
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = "Unknown"
	}
}

defined_text = {
	name = GetGoalOrAchievementDesc
	
	text = {
		trigger = {
			always = yes
			set_temp_variable = { goal_or_achievement_id = v }
		}
		localization_key = "[This.GetGoalOrAchievementDescOtherContext]"
	}
}

defined_text = {
	name = GetGoalOrAchievementDescOtherContext
	
	text = {
		trigger = {
			check_variable = { goal_or_achievement_id = 1 }
		}
		localization_key = GOAL_OR_ACHIEVEMENT_1_DESC
	}
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = "Unknown"
	}
}

defined_text = {
	name = GetGoalOrAchievementIcon
	
	text = {
		trigger = {
			check_variable = { v = 1 }
		}
		localization_key = "GFX_GER_Enabling_Act"
	}
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = "GFX_goal_unknown"
	}
}

defined_text = {
	name = GetGoalOrAchievementIcon
	
	text = {
		trigger = {
			check_variable = { v = 1 }
		}
		localization_key = "GFX_GER_Enabling_Act"
	}
	
	text = {
		trigger = {
			always = yes
		}
		localization_key = "GFX_goal_unknown"
	}
}