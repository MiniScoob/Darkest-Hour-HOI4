
state={
	id=813
	name="STATE_813"

	history={
		owner = ITA
		buildings = {
			infrastructure = 3
			11735 = {
				naval_base = 5
			}

		}
		add_core_of = ITA
		add_core_of = YUG
		1944.6.20 = {
			owner = GER
			controller = GER
		}
		1946.1.1 = {
			owner = YUG
			controller = YUG
		}
		2000.1.1 = {
			owner = CRO
		}		
	}

	provinces={
		11735
	}
	manpower=51000
	buildings_max_level_factor=1.000
	state_category=pastoral
}
