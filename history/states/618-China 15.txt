
state={
	id=618
	name="STATE_618" # Tacheng
	manpower = 371150

	state_category = pastoral

	history={
		owner = SIK
		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		1950.1.1 = {
			owner = PRC
		}
	}

	provinces={
		1708 1783 1844 
	}
}
