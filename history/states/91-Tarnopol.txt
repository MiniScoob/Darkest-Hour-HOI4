state={
	id=91
	name="STATE_91" # Lwow
	manpower = 3227800
	
	state_category = city
	
	history={
		owner = POL
		victory_points = {
			11479 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			air_base = 5
		}
		add_core_of = POL
		add_core_of = UKR
		1940.5.10 = {
			owner = SOV
			controller = SOV
		}
		1944.6.20 = {
			controller = SOV
			GER = {
				set_province_controller = 11479
				set_province_controller = 9558
				set_province_controller = 536
				set_province_controller = 9468
			}
		}
		1946.1.1 = {
			owner = SOV
			controller = SOV
		}
		2000.1.1 = {
			owner = UKR
		}				
	}

	provinces={
		438 491 536 3483 3562 9454 9468 9558 11427 11479 
	}
}
