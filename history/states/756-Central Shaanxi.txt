
state={
	id=756
	name="STATE_756"

	history={

		1933.1.1 = {
			owner = CHI
			add_core_of = CHI
			add_core_of = PRC		
		}
		
		1936.1.1 = {
			owner = NEA
			add_core_of = NEA
			add_core_of = CHI
			add_core_of = PRC			
		}
		1944.6.20 = {
			owner = CHI
			remove_core_of = NEA
		}		
		1950.1.1 = {
			owner = PRC
		}
		buildings = {
			infrastructure = 1
		}
	}

	provinces={
		5007 10733 12156
	}
	manpower=400000
	buildings_max_level_factor=1.000
	state_category=rural
}
