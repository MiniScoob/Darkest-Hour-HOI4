
state={
	id=94
	name="STATE_94"

	history={
		owner = POL
		buildings = {
			infrastructure = 3

		}
		victory_points = {
			3392 1 
		}
		add_core_of = POL
		add_core_of = BLR
		1940.5.10 = {
			owner = SOV
			controller = SOV

		}
		1944.6.20 = {
			owner = POL
			controller = GER

		}
		1946.1.1 = {
			owner = SOV
			controller = SOV

		}
		2000.1.1 = {
			owner = BLR

		}

	}

	provinces={
		535 560 3392 3496 3556 6280 6306 6415 6546 6579 9520 11285 11503 11528 
	}
	manpower=1232200
	buildings_max_level_factor=1.000
	state_category=rural
}
