
state={
	id=36
	name="STATE_36"

	history={
		owner = HOL
		add_core_of = HOL
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			6336 = {
				naval_base = 3

			}

		}
		1940.5.10 = {
			GER = {
				set_province_controller = 6241
				set_province_controller = 9363
				set_province_controller = 9403
				set_province_controller = 6286
				set_province_controller = 9309
				set_province_controller = 11318

			}

		}
		1941.6.22 = {
			owner = HOL
			controller = GER

		}
		1946.1.1 = {
			owner = HOL
			controller = HOL

		}

	}

	provinces={
		6241 6286 6336 9309 9335 9363 9403 11318 
	}
	manpower=2364000
	buildings_max_level_factor=1.000
	state_category=city
}
