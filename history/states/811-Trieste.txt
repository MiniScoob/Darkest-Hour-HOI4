
state={
	id=811
	name="STATE_811"

	history={
		owner = ITA
		victory_points = {
			6626 10
		}
		buildings = {
			infrastructure = 6
			6626 = {
				naval_base = 3
			}

		}
		add_core_of = ITA
		add_core_of = YUG
		1944.6.20 = {
			owner = GER
			controller = GER
		}
		1946.1.1 = {
			owner = ITA
		}	
	}

	provinces={
		6626
	}
	manpower=370000
	buildings_max_level_factor=1.000
	state_category=rural
}
