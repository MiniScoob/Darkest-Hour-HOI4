
state={
	id=819
	name="STATE_819"

	history={
		owner = YUG
		buildings = {
			infrastructure = 3
		}
		add_core_of = YUG
		add_core_of = SER
		1941.6.22 = {
			owner = CRO
			controller = CRO
		}
		1946.1.1 = {
			owner = YUG
			controller = YUG
		}		
	}

	provinces={
		11580
	}
	manpower=63985
	buildings_max_level_factor=1.000
	state_category=pastoral
}
