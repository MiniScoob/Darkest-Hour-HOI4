
state={
	id=271
	name="STATE_271"
	resources={
		rubber=2.000
	}

	history={
		owner = ETH
		buildings = {
			infrastructure = 3
			industrial_complex = 1

		}
		victory_points = {
			5010 5 
		}
		victory_points = {
			7980 1.0 
		}
		add_core_of = ETH
		1939.9.1 = {
			owner = ITA
		}
		1942.11.22 = {
			owner = ETH
		}
	}

	provinces={
		2009 2040 4954 5010 5097 7944 12856 13137 
	}
	manpower=8643566
	buildings_max_level_factor=1.000
	state_category=town
}
