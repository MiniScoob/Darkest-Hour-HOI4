state={
	id=773
	name="STATE_773"
	provinces={
		1155 4073 9947 12049
	}
	history={
		owner = ENG
		buildings = {
			infrastructure = 2
			1155 = {
				naval_base = 2

			}

		}
		add_core_of = EGY
		1960.1.1 = {
			owner = EGY
		}
	}
	manpower=118635
	buildings_max_level_factor=1.000
	state_category=wasteland
}
