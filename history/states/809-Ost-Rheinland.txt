
state={
	id=809
	name="STATE_809"
	resources={
		steel=14.000
	}

	history={
		owner = GER
		victory_points = {
			6469 10 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 1
			industrial_complex = 1
			air_base = 3

		}
		add_core_of = GER
		set_demilitarized_zone = yes
		1936.3.7 = {
			set_demilitarized_zone = no

		}
		1939.1.1 = {
			buildings = {
				air_base = 6
				radar_station = 1

			}

		}
		1946.1.1 = {
			owner = ENG
			controller = ENG

		}
		1950.1.1 = {
			owner = WGR

		}

	}

	provinces={
		529 3512 6469 6570 9482 
	}
	manpower=3584433
	buildings_max_level_factor=1.000
	state_category=large_town
}
