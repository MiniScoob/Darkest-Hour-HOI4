
state={
	id=76
	name="STATE_76"

	history={
		owner = ROM
		buildings = {
			infrastructure = 4
			industrial_complex = 1

		}
		victory_points = {
			6711 5 
		}
		add_core_of = ROM
		1940.5.10 = {
			owner = HUN
			controller = HUN
		}
		1944.6.20 = {
			owner = HUN
			controller = HUN
		}
		1946.1.1 = {
			owner = ROM
			controller = ROM
		}
	}

	provinces={
		713 727 3696 3709 6711 6714 6731 9672 9687 11676 
	}
	manpower=1417301
	buildings_max_level_factor=1.000
	state_category=rural
}
