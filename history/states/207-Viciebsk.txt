
state={
	id=207
	name="STATE_207"

	history={
		owner = SOV
		victory_points = {
			11241 5 
		}
		buildings = {
			infrastructure = 5
			air_base = 5

		}
		add_core_of = SOV
		add_core_of = BLR
		1944.6.20 = {
			GER = {
				set_province_controller = 6249
				set_province_controller = 9323
				set_province_controller = 9241
				set_province_controller = 6220
				set_province_controller = 6371
				set_province_controller = 6326
				set_province_controller = 3219
				set_province_controller = 3331

			}

		}
		1946.1.1 = {
			owner = SOV
			controller = SOV

		}
		2000.1.1 = {
			owner = BLR

		}

	}

	provinces={
		323 3219 3331 6220 6249 6326 6371 9241 9323 11220 11241 
	}
	manpower=1217854
	buildings_max_level_factor=1.000
	state_category=town
}
