
state={
	id=158
	name="STATE_158"
	resources={
		steel=16
		aluminium=10
		chromium=2.000
		tungsten=1.000
	}

	history={
		owner = ITA
		victory_points = {
			6780 5 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 5
			industrial_complex = 1
			air_base = 5

		}
		add_core_of = ITA
		1944.6.20 = {
			owner = RSI
			controller = RSI
		}
		1946.1.1 = {
			owner = ITA
			controller = ITA
		}		
		1939.1.1 = {
			buildings = {
				arms_factory = 7

			}

		}

	}

	provinces={
		611 776 3782 6635 6780 9738 11570 
	}
	manpower=3298476
	buildings_max_level_factor=1.000
	state_category=city
}
