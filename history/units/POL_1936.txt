﻿division_template = {
	name = "Oddział piechoty"         # Infantry Division
	division_names_group = POL_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}	
}

division_template = {
	name = "Dywizja Piechoty z Inżynierami"         # Infantry Division with Engineers
	division_names_group = POL_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
    }
    support = {
        engineer = { x = 0 y = 0 }
    }
}

division_template = {
	name = "Dywizja Piechoty z Artylerią"         # Infantry Division with Artillery
	division_names_group = POL_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
		artillery_brigade = { x = 2 y = 2 }
	}	
}

division_template = {
	name = "Dywizja Piechoty z samolotami"         # Infantry Division with Anti Air
	division_names_group = POL_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		anti_air_brigade = { x = 2 y = 0 }
		anti_air_brigade = { x = 2 y = 1 }
		anti_air_brigade = { x = 2 y = 2 }
	}	
}

division_template = {
	name = "Dywizja Górskiej"             # Mountaineer Division
	division_names_group = POL_MNT_01
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }	
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 2 y = 0 }	
		mountaineers = { x = 2 y = 1 }	
		mountaineers = { x = 2 y = 2 }	
	}
}

division_template = {
	name = "Dywizja Kawalerii"           # Cavalry Division
	division_names_group = POL_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
        cavalry = { x = 0 y = 2 }
        cavalry = { x = 1 y = 0 }
        cavalry = { x = 1 y = 1 }
        cavalry = { x = 1 y = 2 }
	}
}

division_template = {
	name = "Dywizja Kawalerii z Artylerią"           # Cavalry Division with Artillery
	division_names_group = POL_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
        cavalry = { x = 0 y = 2 }
        artillery_brigade = { x = 1 y = 0 }
        artillery_brigade = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Dywizja Kawalerii z Brygadą Pancerną"            # Cavalry Division with Tank brigade
	division_names_group = POL_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
        cavalry = { x = 0 y = 2 }
        medium_armor = { x = 1 y = 0 }
        medium_armor = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Dywizja Kawalerii ze zmotoryzowanym"            # Cavalry Division with Motorised (Armoured Cars)
	division_names_group = POL_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
        cavalry = { x = 0 y = 2 }
        motorized = { x = 1 y = 0 }
        motorized = { x = 1 y = 1 }
	}	
}


units = {
	##### 1 Okreg Wojskowy #####
	division = { # 8 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 3544 # Warsaw
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 9 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 3544 # Warsaw
		division_template = "Dywizja Piechoty z samolotami"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 28 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 28
		}
		location = 3544 # Warsaw
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 1 Dywizja Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 3544 # Warsaw
		division_template = "Dywizja Kawalerii z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Podlaska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 105
		}
		location = 3544 # Warsaw
		division_template = "Dywizja Kawalerii z Brygadą Pancerną"
		start_experience_factor = 0.1
		start_equipment_factor = 0.25
	}

	division = { # Mazowiecka Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 103
		}
		location = 3544 # Warsaw
		division_template = "Dywizja Kawalerii ze zmotoryzowanym"
		start_experience_factor = 0.1
		start_equipment_factor = 0.25
	}
	##### 2 Okreg Wojskowy #####
	division = { # 3 Dywizja Piechoty Legionów
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6484 # Lublin
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 13 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 6484 # Lublin
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 27 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 27
		}
		location = 6484 # Lublin
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Wolynska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 111
		}
		location = 6484 # Lublin
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}

	division = { # 41 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 41
		}
		location = 6484 # Lublin
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}
	##### 3 Okreg Wojskowy #####
	division = {
        name = "1 Dywizja Piechoty Legionów"
        location = 3393 # Grodno
        division_template = "Dywizja Piechoty z Artylerią"
        start_experience_factor = 0.2
        start_equipment_factor = 0.5
	}
	
	division = { # 19 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 19
		}
		location = 3393 # Grodno
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 29 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 29
		}
		location = 3393 # Grodno
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.45
	}

	division = { # Suwalska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 108
		}
		location = 3393 # Grodno
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.25
	}

	division = { # Wilenska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 110
		}
		location = 3393 # Grodno
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.25
	}

	division = { # 39 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 39
		}
		location = 3393 # Grodno
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}

	division = { # 35 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 35
		}
		location = 3393 # Grodno
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}

	division = { # 33 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 33
		}
		location = 3393 # Grodno
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}
	##### 4 Okreg Wojskowy #####
	division = { # 7 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 9508 # Lodz
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 10 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 9508 # Lodz
		division_template = "Dywizja Piechoty z Inżynierami"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 26 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 26
		}
		location = 9508 # Lodz
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = {
        name = "Lodzka Brygada Kawalerii"
        location = 9508 # Lodz
        division_template = "Dywizja Kawalerii"
        start_experience_factor = 0.1
        start_equipment_factor = 0.3
	}
	
	division = { # 45 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 45
		}
		location = 9508 # Lodz
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}
	##### 5 Okreg Wojskowy #####
	division = { # 6 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 9427 # Krakow
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 21 Dywizja Piechoty Górskiej
		division_name = {
			is_name_ordered = yes
			name_order = 21
		}
		location = 9427 # Krakow
		division_template = "Dywizja Górskiej"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 23 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 23
		}
		location = 9427 # Krakow
		division_template = "Dywizja Piechoty z Inżynierami"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Krakowska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 101
		}
		location = 9427 # Krakow
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}

	division = { # 55 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 55
		}
		location = 9427 # Krakow
		division_template = "Dywizja Górskiej"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}
	##### 6 Okreg Wojskowy #####
	division = { # 5 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 11479 # Lwow
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 11 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 11479 # Lwow
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 12 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
		location = 11479 # Lwow
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Podolska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 106
		}
		location = 11479 # Lwow
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}

	division = { # 36 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 36
		}
		location = 11479 # Lwow
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}
	##### 7 Okreg Wojskowy #####
	division = { # 14 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}
		location = 6558 # Poznan
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 17 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}
		location = 6558 # Poznan
		division_template = "Dywizja Piechoty z Inżynierami"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 25 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 25
		}
		location = 6558 # Poznan
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Wielkopolska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 109
		}
		location = 6558 # Poznan
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
	##### 8 Okreg Wojskowy #####
	division = { # 4 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 3295 # Torun
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 15 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 15
		}
		location = 3295 # Torun
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 16 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 16
		}
		location = 3295 # Torun
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Pomorska Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 107
		}
		location = 3295 # Torun
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
	##### 9 Okreg Wojskowy #####
	division = { # 18 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 18
		}
		location = 3392 # Brest-Litovsk
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 20 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 20
		}
		location = 3392 # Brest-Litovsk
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 30 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 30
		}
		location = 3392 # Brest-Litovsk
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Nowogródzka Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 104
		}
		location = 3392 # Brest-Litovsk
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}

	division = { # 38 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 38
		}
		location = 3392 # Brest-Litovsk
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}

	division = { # 44 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 44
		}
		location = 3392 # Brest-Litovsk
		division_template = "Oddział piechoty"
		start_experience_factor = 0.1
		start_equipment_factor = 0.05
	}
	##### 10 Okreg Wojskowy #####
	division = {
		name = "2 Dywizja Piechoty Legionów"
		location = 9494 # Przemysl
		division_template = "Dywizja Piechoty z Artylerią"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 22 Dywizja Piechoty Górskiej
		division_name = {
			is_name_ordered = yes
			name_order = 22
		}
		location = 9494 # Przemysl
		division_template = "Dywizja Górskiej"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # 24 Dywizja Piechoty
		division_name = {
			is_name_ordered = yes
			name_order = 24
		}
		location = 9494 # Przemysl
		division_template = "Oddział piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.5
	}

	division = { # Kresowa Brygada Kawalerii
		division_name = {
			is_name_ordered = yes
			name_order = 102
		}
		location = 9494 # Przemysl
		division_template = "Dywizja Kawalerii"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3
	}
	######## NAVAL OOB ########
	fleet = {
		name = "Polska Flota Wojenna"
		naval_base = 362 # Danzig
		task_force = {
			name = "Polska Flota Wojenna"
			location = 362 # Danzig
			ship = { name = "Flotylla Niszczycieli" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = POL } } }	
			ship = { name = "Flotylla Dozorowców" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = POL } } }
			ship = { name = "1 Flotylla Okretow Podwodnych" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = POL } } }
		}
	}
}

##### Wojska Lotnicze i Obrony Powietrzne #####
air_wings = {
	10 = {
        # 3 Korpus Lotnictwa Mysliwskiego
        Fighter_equipment_1933 = { # PZL P.11
            owner = "POL"
            amount = 95
        }
        # 4 Korpus Lotnictwa Szturmowego
        Fighter_equipment_1933 = { # PZL P.11
            owner = "POL"
            amount = 50
        }
        # 15 Dywizja Lotnictwa Bombowego
        Tactical_Bomber_equipment_1933 = { # Amiot 143
            owner = "POL"
            amount = 28
        }
    }
}

#########################
## STARTING PRODUCTION ##
#########################
instant_effect = {
    add_equipment_production = {
        equipment = {
            type = Small_Arms_equipment_1936
            creator = "POL"
        }
        requested_factories = 2
        progress = 0.38
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = support_equipment_1
            creator = "POL"
        }
        requested_factories = 1
        progress = 0.32
        efficiency = 100
    }
    
    add_equipment_production = {
        equipment = {
            type = artillery_equipment_1
            creator = "POL"
        }
        requested_factories = 2
        progress = 0.24
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = anti_air_equipment_1
            creator = "POL"
        }
        requested_factories = 1
        progress = 0.22
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = Tank_equipment_1926
            creator = "POL"
        }
        requested_factories = 1
        progress = 0.19
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = DD_equipment_1933
            creator = "POL"
        }
        requested_factories = 2
        progress = 0.2
        amount = 3
    }
}