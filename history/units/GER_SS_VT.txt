﻿
division_template = {
	name = "SS Schnelltruppen"
	priority = 2
	division_names_group = GER_SS_01
	regiments = {
		SS_motorized = { x = 0 y = 0 }
		SS_motorized = { x = 0 y = 1 }
		SS_motorized = { x = 0 y = 2 }

		SS_motorized = { x = 1 y = 0 }
		SS_motorized = { x = 1 y = 1 }
		SS_motorized = { x = 1 y = 2 }

		SS_motorized = { x = 2 y = 0 }
		SS_motorized = { x = 2 y = 1 }
		SS_motorized = { x = 2 y = 2 }
	}
	support = {
		recon = { x = 0 y = 2 }
	}
	priority = 2
}

units = {
	division= {
		name = "SS-Verfügungstruppe"
		location = 11505
		division_template = "SS Schnelltruppen"
		start_experience_factor = 0.3
	}
}