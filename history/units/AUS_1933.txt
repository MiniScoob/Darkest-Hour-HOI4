﻿division_template = {
	name = "Infanterie-Division"
	division_names_group = AUS_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }   # Pioneer Bn
	}
}

division_template = {
	name = "Alpenjäger Division"
	division_names_group = AUS_MNT_01
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }   # Pioneer Bn
	}
}

division_template = {
	name = "Schnelle-Division"  		# Schnelle-Division (cavalry)
	division_names_group = AUS_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }      # Light tank (tankette) bn
	}
}
division_template = {
	name = "Brigadekommando"
	division_names_group = AUS_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
	support = {
		
	}
}

units = {
	##### Österreichisches Bundesheer #####
	division= {	
		name = "Brigadekommando Nr.1"
		location = 11666  # Vienna
		division_template = "Brigadekommando"
		start_experience_factor = 0.1
		start_equipment_factor = 0.3

	}
	division= {	
		name = "Brigadekommando Nr.3"
		location = 11651 # St Poelten
		division_template = "Brigadekommando"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}

	# II Korps
	division= {
		name = "Brigadekommando Nr.4"
		location = 9665 # Linz
		division_template = "Brigadekommando"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {
		name = "Brigadekommando Nr.5"			
		location = 9648  # Graz
		division_template = "Brigadekommando"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {
		name = "Brigadekommando Nr.6"
		location = 673  # Innsbruck
		division_template = "Brigadekommando"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
	division= {	
		name = "Brigadekommando Nr.2"
		location = 688  # Salzburg
		division_template = "Brigadekommando"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3

	}
}

air_wings = {
	### Luftstreitkräfte -- Vienna (full air force, consolidated), CR.32 aircraft
	4 = {
		Fighter_equipment_1933 =  {
			owner = "AUS" 
			creator = "ITA"
			amount = 42
		}
	}
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1918
			creator = "AUS"
		}
		requested_factories = 1
		progress = 0.54
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "AUS"
		}
		requested_factories = 1
		progress = 0.74
		efficiency = 100
	}
}