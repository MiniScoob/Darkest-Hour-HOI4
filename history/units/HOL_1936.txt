﻿division_template = {
	name = "Infanterie Divisie" # Infantry Division
	division_names_group = HOL_INF_01 
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Infanterie Divisie met ingenieurs" # Infantry Division with Engineers
	division_names_group = HOL_INF_01 
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
    }
    support = {
        engineer = { x = 0 y = 0 }
    }
}

division_template = {
	name = "Infanteriedivisie met antivliegtuig" # Infantry Division with Anti air
	division_names_group = HOL_INF_01 
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		anti_air_brigade = { x = 2 y = 0 }
		anti_air_brigade = { x = 2 y = 1 }
		anti_air_brigade = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Infanterie Divisie met Artillerie" # Infantry Division with Artillery
	division_names_group = HOL_INF_01 
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		artillery_brigade = { x = 2 y = 0 }
		artillery_brigade = { x = 2 y = 1 }
		artillery_brigade = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Infanteriedivisie met gemotoriseerd" # Infantry Division with Motorised (Armoured Cars)
	division_names_group = HOL_INF_01 
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }
	}
}

units = {
	##### Ie Legerkorps #####
	division = { # 1e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 9498 # Rotterdam
        division_template = "Infanteriedivisie met antivliegtuig"
        start_experience_factor = 0.2
        start_equipment_factor = 0.8
	}

	division = { # 5e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 9498 # Rotterdam
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
	}
	##### IIe Legerkorps #####
	division = { # 2e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6241 # Arnhem
        division_template = "Infanterie Divisie met Artillerie"
        start_experience_factor = 0.2
        start_equipment_factor = 0.8
	}

	division = { # 6e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 6241 # Arnhem
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
	}
	##### IIIe Legerkorps #####
	division = { # 3e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6496 # Eindhoven
        division_template = "Infanterie Divisie met ingenieurs"
        start_experience_factor = 0.2
        start_equipment_factor = 0.8
	}

	division = { # 7e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 6496 # Eindhoven
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
	}
	##### IVe Legerkorps #####
	division = { # 4e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 11456 # Nijmegen
        division_template = "Infanteriedivisie met gemotoriseerd"
        start_experience_factor = 0.2
        start_equipment_factor = 0.8
	}

	division = { # 8e Divisie
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 11456 # Nijmegen
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
	}


	##### NAVAL UNITS #####
	fleet = {
		name = "Koninklijke Vloot"
		naval_base = 3314 # Amsterdam
		task_force = {
			name = "Koninklijke Vloot"
			location = 3314 # Amsterdam
			ship = { name = "HrMs De Ruyter" definition = light_cruiser equipment = { CL_equipment_1933 = { amount = 1 owner = HOL } }	}		
			ship = { name = "HrMs Java" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = HOL } } }
			ship = { name = "HrMs Sumatra" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = HOL } } }
			ship = { name = "1e Torpedobootjager Smaldeel" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = HOL } } }		
			ship = { name = "2e Torpedobootjager Smaldeel" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = HOL } } }		
			ship = { name = "3e Torpedobootjager Smaldeel" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = HOL } } }
		}
		task_force = {
			name = "3e Koninklijke Vloot"
			location = 3314 # Amsterdam
			ship = { name = "1e Onderzeeboot Smaldeel" definition = submarine equipment = { SS_equipment_1916 = { amount = 1 owner = HOL } } }
			ship = { name = "2e Onderzeeboot Smaldeel" definition = submarine equipment = { SS_equipment_1916 = { amount = 1 owner = HOL } } }
			ship = { name = "3e Onderzeeboot Smaldeel" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = HOL } } }
			ship = { name = "4e Onderzeeboot Smaldeel" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = HOL } } }
			ship = { name = "5e Onderzeeboot Smaldeel" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = HOL } } }
		}
	}										
}

### Air Wings
air_wings = {
    7 = {
        # 1e Luchtvaartregiment
        Fighter_equipment_1933 = { # Fokker D.VII
            owner = "HOL"
            amount = 30
        }
         # 1e Luchtvaartgroep
         Fighter_equipment_1933 = { # Improved WW1 Fighter
         owner = "HOL"
         amount = 55
        }
        # 2e Luchtvaartgroep
        Naval_Bomber_equipment_1936 = { # Fokker T.IV
            owner = "HOL"
            amount = 18
        }
    }
}

### Starting Production ###
instant_effect = {
    add_equipment_production = {
        equipment = {
            type = Small_Arms_equipment_1936
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.64
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = support_equipment_1
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.7
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = artillery_equipment_1
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.49
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = anti_air_equipment_1
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.47
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = DD_equipment_1912
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.32
        amount = 4
    }

    add_equipment_production = {
        equipment = {
            type = SS_equipment_1922
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.49
        amount = 8
    }

    add_equipment_production = {
        equipment = {
            type = CA_equipment_1906
            creator = "HOL"
        }
        requested_factories = 1
        Progress = 0.24
        amount = 1
    }
}