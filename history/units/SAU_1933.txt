﻿division_template = {
	name = "Alfurqat Almasha"		# 'Infantry Division' - Represents local tribal levies (militia)

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
}

division_template = {
	name = "Alfurqat Alfursan"		# Camel-mounted levies (militia)

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Qasam Alhamia"		# Garrison Division

	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
		garrison = { x = 1 y = 0 }
		garrison = { x = 1 y = 1 }
    }
    priority = 0
}

units = {
    ##### Nejd Command #####
    division = {
        name = "Haris Milkiun"
        location = 12727 # Riyadh
        division_template = "Alfurqat Almasha"
        start_experience_factor = 0.1
        start_equipment_factor = 0.15
    }

    division = {
        name = "Faylaq Aljamal"
        location = 12727 # Riyadh
        division_template = "Alfurqat Alfursan"
        start_experience_factor = 0.1
    }

    division = {
        name = "Riad Hamia"
        location = 12727 # Riyadh
        division_template = "Qasam Alhamia"
        start_experience_factor = 0.1
        start_equipment_factor = 0.9
    }
    ##### Hejaz Command #####
    division = {
        name = "Almadinat Almunawarat Hamia"
        location = 12758 # Medina
        division_template = "Qasam Alhamia"
        start_experience_factor = 0.1
        start_equipment_factor = 0.9
    }
    ##### Asir Command #####
    division = {
        name = "Hamiat Makatan"
        location = 5037 # Jeddah (Mecca)
        division_template = "Qasam Alhamia"
        start_experience_factor = 0.1
        start_equipment_factor = 0.9
    }
}
### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1918
			creator = "SAU"
		}
		requested_factories = 1
		progress = 0.24
		efficiency = 100
	}
}