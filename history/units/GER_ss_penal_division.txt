﻿division_template = {
	name = "SS-Parachute Battalion"

	regiments = {
		paratrooper = { x = 0 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 }
		recon = { x = 0 y = 1 }
		logistics_company = { x = 0 y = 2 }
	}
}

units = {
	division= {
		name = "500th SS-Parachute Battalion"
		location = 6521
		division_template = "SS-Parachute Battalion"
		start_experience_factor = 1.0
	}
	division= {
		name = "600th SS-Parachute Battalion"
		location = 6521
		division_template = "SS-Parachute Battalion"
		start_experience_factor = 1.0
		start_equipment_factor = 0.01
	}
}