﻿division_template = {
	name = "División de Infanteria"		# Maintained at reserve levels during peacetime

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
}

units = {
    ##### Ejercito Ecuadoriano #####
    division = {
        name = "1a Brigada de Infantería"
        location = 12798 # San Francisco de (Quito)
        division_template = "División de Infanteria"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
    }

    division = {
        name = "2a Brigada de Infantería"
        location = 12798 # San Francisco de (Quito)
        division_template = "División de Infanteria"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
    }

    division = {
        name = "3a Brigada de Infantería"
        location = 12798 # San Francisco de (Quito)
        division_template = "División de Infanteria"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
    }

    division = {
        name = "4a Brigada de Infantería"
        location = 12798 # San Francisco de (Quito)
        division_template = "División de Infanteria"
        start_experience_factor = 0.1
        start_equipment_factor = 0.1
    }
    ##### navy oob #####
    fleet = {
        name = "Armada del Ecuador"
        naval_base = 8252 # Santiago de Guay (Guayquil)
        task_force = {
            name = "Armada del Ecuador"
            location = 8252 # Santiago de Guay (Guayquil)
            ship = { name = "BAE Libertador Bolivar" definition = destroyer equipment = { DD_equipment_1900 = { amount = 1 owner = ECU } } }
        }       
    }
}
### Starting Production ###
instant_effect = {
    add_equipment_production = {
        equipment = {
            type = Small_Arms_equipment_1918
            creator = "ECU"
        }
        requested_factories = 1
        progress = 0.55
        efficiency = 100
    }
}