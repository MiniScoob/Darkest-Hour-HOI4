﻿##############################################
## German Templates
##############################################
division_template = {
	name = "Infanterie-Division"
	
	division_names_group = GER_Inf_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}
division_template = {
	name = "Panzer-Division"
	
	division_names_group = GER_Arm_01
	regiments = {
		medium_armor = { x = 0 y = 0 }
		medium_armor = { x = 0 y = 1 }
		medium_armor = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Infanterie-Division (mot.)"
	
	division_names_group = GER_MOT_01
	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
		
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
		
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
		motorized = { x = 2 y = 2 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
	priority = 2
}
division_template = {
	name = "Gebirgs-Brigade"
	
	division_names_group = GER_Mnt_01
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
	}
}
division_template = {
	name = "Kavallerie-Brigade"
	
	division_names_group = GER_Cav_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }
	}
}
##############################################
## German Land Units
##############################################
units = {
	# Wehrkreiskommando I
	division= { # 1. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6332 # Koenigsberg
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Wehrkreiskommando II
	division= { # 2. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6282 # Stettin
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Wehrkreiskommando III
	division= { # 3. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6521 # Berlin
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Wehrkreiskommando IV
	division= { # 4. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 514 # Dresden
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Wehrkreiskommando V
	division= { # 5. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 9517 # Stuttgart
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Wehrkreiskommando VI
	division= { # 6. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 11388 # Muenster
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Wehrkreiskommando VII
	division= { # 7. Infanterie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 692 # Munich
		division_template = "Infanterie-Division"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	
	# Gruppenkommando 1
	division= { # 1. Kavallerie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6521 # Berlin
		division_template = "Kavallerie-Brigade"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	division= { # 2. Kavallerie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6521 # Berlin
		division_template = "Kavallerie-Brigade"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}
	# Gruppenkommando 2
	division= { # 3. Kavallerie-Division
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 3561 # Kassel
		division_template = "Kavallerie-Brigade"
		start_experience_factor = 0.0
		start_equipment_factor = 0.75
	}	
}
##############################################
## Naval OOB
##############################################
units = {
	fleet = {
		name = "Reichsmarine"
		naval_base = 241 # Wilhemshaven
		task_force = {
			name = "Reichsmarine"
			location = 241 # Wilhemshaven
			ship = { name = "Schlesien" definition = battleship equipment = { BB_equipment_1895 = { amount = 1 owner = GER } } }
			ship = { name = "Schleswig-Holstein" definition = battleship equipment = { BB_equipment_1895 = { amount = 1 owner = GER } } }	
			ship = { name = "Hessen" definition = light_cruiser equipment = { BB_equipment_1895 = { amount = 1 owner = GER } } }
			
			ship = { name = "Leipzig" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Königsberg" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Karlsruhe" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Köln" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Emden" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = GER } } }
			ship = { name = "Berlin" definition = light_cruiser equipment = { CL_equipment_1900 = { amount = 1 owner = GER } } }
			
			ship = { name = "1. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1900 = { amount = 1 owner = GER } } }
			ship = { name = "2. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1912 = { amount = 1 owner = GER } } }
			ship = { name = "3. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1912 = { amount = 1 owner = GER } } }
			ship = { name = "4. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "5. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "6. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = GER } } }
		}
	}	
}
##############################################
## German Air Units
##############################################
air_wings = {	
	### Kampffliegerschule - Berlin
	64 = { 
		# Kampffliegerschule (Fokker D.XIII)	
		Fighter_equipment_1933 =  {
			owner = "GER" 
			amount = 50
		}
		name = "Kampffliegerschule"	
	}
	### Deutsche Luft Hansa - Berlin
	64 = { 
		# Blitzstrecken	(Do 11)	
		Tactical_Bomber_equipment_1933 = {
			owner = "GER" 
			amount = 60
		}
		name = Blitzstrecken
		# Deutsche Luft Hansa (Ju W 34)
		transport_plane_equipment_1 = {
			owner = "GER" 
			amount = 4 
		}
		name = "Luft Hansa"		
	}
}
##############################################
## German Production
##############################################
instant_effect = {
	# Kar 98k
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1918
			creator = "GER"
		}
		requested_factories = 10
		progress = 0.1
		efficiency = 50
	}
	# Support Equipment
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "GER"
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 50
	}
	# leFH18
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 50
	}
	# Pz IIs
	add_equipment_production = {
		equipment = {
			type = Light_Tank_equipment_1933
			creator = "GER"
		}
		requested_factories = 2
		progress = 0.4
		efficiency = 50
	}
	# Opel Blitz
	add_equipment_production = {
		equipment = {
			type = truck_equipment_1936
			creator = "GER"
		}
		requested_factories = 2
		progress = 0.4
		efficiency = 50
	}
	# Bf-109s
	add_equipment_production = {
		equipment = {
			type = Fighter_equipment_1933
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.15
		efficiency = 50
	}
	# Ju-87s
	add_equipment_production = {
		equipment = {
			type = CAS_equipment_1
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.2
		efficiency = 50
	}
	# He-111s
	add_equipment_production = {
		equipment = {
			type = Tactical_Bomber_equipment_1936
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.1
		efficiency = 50
	}
	
	#########################################################################
	#  Ships Under Contruction
	#########################################################################	
	## The Formula used is X=100-(100Y)/T ; X= Progress, Y= Time Left, T= Total Building time
	# CA: "Admiral Graf Spee"
	add_equipment_production = {
		equipment = {
			type = CA_equipment_1933
			creator = "GER"
		}
		name = "Admiral Graf Spee"
		requested_factories = 2
		progress = 0.99
		amount = 1
	}
	# CA: "Admiral Hipper"
	add_equipment_production = {
		equipment = {
			type = CA_equipment_1936
			creator = "GER"
		}
		name = "Admiral Hipper"
		requested_factories = 2
		progress = 0.05
		amount = 1
	}
	# CA: "Blücher"
	add_equipment_production = {
		equipment = {
			type = CA_equipment_1936
			creator = "GER"
		}
		name = "Blücher"
		requested_factories = 2
		progress = 0.00
		amount = 1
	}
	# BC : "Gneisenau"
	add_equipment_production = {
		equipment = {
			type = BC_equipment_1933
			creator = "GER"
		}
		name = "Gneisenau"
		requested_factories = 3
		progress = 0.26
		amount = 1
	}
	# BC:  "Scharnhorst"
	add_equipment_production = {
		equipment = {
			type = BC_equipment_1933
			creator = "GER"
		}
		name = "Scharnhorst"
		requested_factories = 3
		progress = 0.18
		amount = 1
	}
	# DD: Zerstörergeschwader 7,8 & 9
	add_equipment_production = {
		equipment = {
			type = DD_equipment_1933
			creator = "GER"
		}
		name = "Zerstörergeschwader 7"
		requested_factories = 1
		progress = 0.11
		amount = 3
	}

}
