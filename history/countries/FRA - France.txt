﻿#########################################################################
# French Republic - 1933
#########################################################################
1933.1.1 = {
	capital = 16
	set_stability = 0.5
	set_war_support = 0.15
	oob = "FRA_1933"
	set_research_slots = 4
	set_convoys = 300
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 0
		Small_Arms_1936 = 1
		tech_support = 1
		tech_recon = 1
		tech_mountaineers = 1
		truck_1936 = 1
		gw_artillery = 1
		interwar_antiair = 1

		Fighter_1933 = 1
		Naval_Bomber_1936 = 1
		cv_Fighter_1933 = 1
		Torpedo_bomber_1936 = 1
		Tactical_Bomber_1933 = 1
		Tactical_Bomber_1936 = 1
		CAS_1936 = 1
		Strategic_Bomber_1933 = 1
		# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
		Tank_1926 = 1
		Light_Tank_1934 = 1	
		#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1	
		
		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		CL_1922 = 1
		CL_1933 = 1	
		CL_1936 = 1
		
		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1
		CA_1922 = 1
		CA_1933 = 1	
		CA_1936 = 1	
		
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1	
		SS_1933 = 1	
		SS_1936 = 1
		
		HSS_1933 = 1
		
		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1
		BC_1933 = 1
		
		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1
		BB_1906 = 1
		BB_1912 = 1
		BB_1916 = 1
		BB_1922 = 1	
		BB_1933 = 1
		
		CV_1912 = 1
		CV_1916 = 1
		
		CV_1922 = 1	
		
		transport = 1

		trench_warfare = 1
		fleet_in_being = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = democratic
		last_election = "1932.5.1"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 1.5
		authoritarian = 1.2
		democratic = 70.5
		socialist = 6.9
		communist = 19.9
	}
	add_ideas = {
		FRA_Recovery_from_the_Great_Depression
		FRA_Huge_Gold_Buyage
		FRA_Rural_Population
		FRA_1933_Recovery_from_ww1
		FRA_disjointed_government
		FRA_protected_by_the_maginot_line
	# Laws & Policies
		limited_conscription
	# Cabinet
		FRA_HoG_Albert_Lebrun
		FRA_FM_Pierre_Etienne_Flandin
		FRA_AM_Vincent_Auriol
		FRA_MoS_Marx_Dormoy
		FRA_HoI_Joseph_Paganon
	# Military Staff
		FRA_CoStaff_Maurice_Gamelin
		FRA_CoArmy_Maurice_Gamelin
		FRA_CoNavy_JeanFrancois_Darlan
		FRA_CoAir_Victor_Denain	
	}
	######################
	# Diplomacy
	#######################
	# Subjects
	set_autonomy = {
		target = VIN
		autonomous_state = autonomy_colony
		freedom_level = 0.35
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Jacques Doriot"
		desc = ""
		picture = "P_F_Jacques_Doriot.tga"
		expire = "1965.1.1"
		ideology = national_socialism
		traits = {}
	}
	# Authoritarianism
	create_country_leader = {
		name = "Charles Maurras"
		desc = ""
		picture = "P_A_Charles_Maurras.tga"
		expire = "1965.1.1"
		ideology = monarchism
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Edouard Daladier"
		desc = ""
		picture = "P_D_Edouard_Daladier.tga"
		expire = "1938.1.1"
		ideology = social_liberalism
		traits = {
			POSITION_Prime_Minister
			HoG_Naive_Optimist
		}
	}
	# Socialism
	create_country_leader = {
		name = "Marceau Pivert"
		desc = ""
		picture = "P_S_Marceau_Pivert.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Communism
	create_country_leader = {
		name = "Maurice Thorez"
		desc = ""
		picture = "P_C_Maurice_Thorez.tga"
		expire = "1965.1.1"
		ideology = stalinism
		traits = {}
	}	
}
#########################################################################
# French Republic - 1936
#########################################################################
1936.1.1 = {
	capital = 16
	set_stability = 0.5
	set_war_support = 0.15
	oob = "FRA_1936"
	set_research_slots = 4
	set_convoys = 300
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		Small_Arms_1936 = 1
		tech_support = 1
		tech_recon = 1
		tech_mountaineers = 1
		truck_1936 = 1
		gw_artillery = 1
		interwar_antiair = 1

		Fighter_1933 = 1
		Fighter_1936 = 1
		Naval_Bomber_1936 = 1
		cv_Fighter_1933 = 1
		Torpedo_bomber_1936 = 1
		Tactical_Bomber_1933 = 1
		Tactical_Bomber_1936 = 1
		CAS_1936 = 1
		Strategic_Bomber_1933 = 1
		# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
		Tank_1926 = 1
		Light_Tank_1934 = 1	
		#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1	
		
		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		CL_1922 = 1
		CL_1933 = 1	
		CL_1936 = 1
		
		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1
		CA_1922 = 1
		CA_1933 = 1	
		CA_1936 = 1	
		
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1	
		SS_1933 = 1	
		SS_1936 = 1
		
		HSS_1933 = 1
		
		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1
		BC_1933 = 1
		
		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1
		BB_1906 = 1
		BB_1912 = 1
		BB_1916 = 1
		BB_1922 = 1	
		BB_1933 = 1
		
		CV_1912 = 1
		CV_1916 = 1
		
		CV_1922 = 1	
		
		transport = 1

		trench_warfare = 1
		fleet_in_being = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = democratic
		last_election = "1932.5.1"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 1.5
		authoritarian = 1.2
		democratic = 70.5
		socialist = 6.9
		communist = 19.9
	}	
	add_ideas = {
		FRA_disjointed_government
		FRA_victors_of_wwi
		FRA_protected_by_the_maginot_line
		FRA_Recovery_from_the_Great_Depression
	# Laws & Policies
		limited_conscription
	# Cabinet
		FRA_HoG_Albert_Sarraut
		FRA_FM_Pierre_Etienne_Flandin
		FRA_AM_Vincent_Auriol
		FRA_MoS_Marx_Dormoy
		FRA_HoI_Joseph_Paganon
	# Military Staff
		FRA_CoStaff_Maurice_Gamelin
		FRA_CoArmy_Maurice_Gamelin
		FRA_CoNavy_JeanFrancois_Darlan
		FRA_CoAir_Victor_Denain	
	}

#######################
# Subjects
#######################
	set_autonomy = {
		target = VIN
		autonomous_state = autonomy_colony
		freedom_level = 0.35
	}
#######################
# Leaders
#######################
	# Fascism
	create_country_leader = {
		name = "Jacques Doriot"
		desc = ""
		picture = "P_F_Jacques_Doriot.tga"
		expire = "1965.1.1"
		ideology = national_socialism
		traits = {}
	}
	# Authoritarianism
	create_country_leader = {
		name = "Charles Maurras"
		desc = ""
		picture = "P_A_Charles_Maurras.tga"
		expire = "1965.1.1"
		ideology = monarchism
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Pierre Laval"
		desc = ""
		picture = "P_D_Pierre_Laval.tga"
		expire = "1938.1.1"
		ideology = social_liberalism
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Marceau Pivert"
		desc = ""
		picture = "P_S_Marceau_Pivert.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Communism
	create_country_leader = {
		name = "Maurice Thorez"
		desc = ""
		picture = "P_C_Maurice_Thorez.tga"
		expire = "1965.1.1"
		ideology = stalinism
		traits = {}
	}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Maurice Gamelin"
	picture = "M_Maurice_Gamelin.tga"
	traits = { defensive_doctrine old_guard inflexible_strategist career_officer }
	skill = 2
	attack_skill = 1
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 1
}

create_field_marshal = {
	name = "Maxime Weygand"
	picture = "M_Maxime_Weygand.tga"
	traits = { defensive_doctrine old_guard inflexible_strategist career_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_field_marshal = {
	name = "Alphonse Georges"
	picture = "M_Alphonse_Georges.tga"
	traits = { defensive_doctrine old_guard trait_cautious }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 2
}

create_corps_commander = {
	name = "Jean de Lattre de Tassigny"
	picture = "M_Jean_de_Lattre_de_Tassigny.tga"
	traits = { trickster brilliant_strategist cavalry_officer war_hero }
	skill = 4
	attack_skill = 4
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Alphonse Juin"
	picture = "M_Alphonse_Juin.tga"
	traits = { armor_officer career_officer trait_cautious  }
	skill = 4
	attack_skill = 3
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 4
}

create_corps_commander = {
	name = "Charles De Gaulle"
	picture = "M_Charles_De_Gaulle.tga"
	traits = {  armor_officer harsh_leader media_personality }
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Philippe Leclerc"
	picture = "M_Philippe_Leclerc.tga"
	traits = {  armor_officer trait_reckless career_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Henri Giraud"
	picture = "M_Henri_Giraud.tga"
	traits = {  trickster trait_cautious career_officer infantry_officer }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 2
}

create_corps_commander = {
	name = "Charles Huntziger"
	picture = "M_Charles_Huntzinger.tga"
	traits = { old_guard infantry_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}


create_navy_leader = {
	name = "François Darlan"
	picture = "M_Francois_Darlan.tga"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Jean-Marie Charles Abrial"
	picture = "M_Jean_Marie_Charles_Abrial.tga"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Jean-Pierre Esteva"
	picture = "M_Jean-Pierre_Esteva.tga"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "René-Émile Godfroy"
	picture = "M_Rene_Emile_Godfroy.tga"
	traits = { old_guard_navy spotter }
	skill = 2
}

create_navy_leader = {
	name = "Jean de Laborde"
	picture = "M_Jean_de_Laborde.tga"
	traits = {  }
	skill = 1
}
##########################################################
# French Republic - 1944
##########################################################
1944.6.20 = {
	oob = "FRA_1944"
	set_cosmetic_tag = FRA_FREE
	set_politics = {
		ruling_party = democratic
		last_election = "1932.5.1"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 1.5
		authoritarian = 1.2
		democratic = 70.5
		socialist = 4.5
		communist = 17.9
	}	

	# Authoritarianism
	create_country_leader = {
		name = "Charles De Gaulle"
		desc = ""
		picture = "M_Charles_De_Gaulle.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = {}
	}
}
##########################################################
# Provisional Government of the French Republic - 1946
##########################################################
1946.1.1 = {

	drop_cosmetic_tag = yes
	set_politics = {
		ruling_party = democratic
		last_election = "1932.5.1"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 1.5
		authoritarian = 1.2
		democratic = 70.5
		socialist = 4.5
		communist = 17.9
	}

	# Authoritarianism
	create_country_leader = {
		name = "Charles De Gaulle"
		desc = ""
		picture = "M_Charles_De_Gaulle.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = {}
	}
}