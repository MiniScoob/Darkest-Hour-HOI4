﻿capital = 327

oob = "PHI_1936"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 1
	tech_support = 1
	tech_engineers = 1
	gw_artillery = 1
	Fighter_1933 = 1
	Tactical_Bomber_1933 = 1
}

set_convoys = 5

set_politics = {
	ruling_party = democratic
	last_election = "1935.9.15"
	election_frequency = 72
	elections_allowed = yes
}
set_popularities = {
	fascist = 0
	authoritarian = 17
	democratic = 83
	socialist = 0
	communist = 0
}
####################################################
# Philippines Leaders
####################################################
# Conservatism
create_country_leader = {
	name = "Manuel Luis Quezón"
	desc = "POLITICS_MANUEL_LUIS_QUEZON_DESC"
	picture = "P_C_Manuel_Luis_Quezon.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}

1946.1.1 = {
	white_peace = MON
	white_peace = ROM
	white_peace = NOR
}
