﻿#########################################################################
# Austria - 1933
#########################################################################
1933.1.1 = {
	capital = 4
	oob = "AUS_1933"
	set_research_slots = 3
	set_stability = 0.50
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		Small_Arms_1936 = 1
		tech_recon = 1
		tech_engineers = 1
		tech_support = 1		
		tech_mountaineers = 1
		Fighter_1933 = 1
		Strategic_Bomber_1933 = 1
	# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = fascist
		last_election = "1930.11.9"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 50
		authoritarian = 0
		democratic = 38
		socialist = 12
		communist = 0
	}	
	add_ideas = {
	# Cabinet
		AUS_HoG_Kurt_Schuschnigg
		AUS_FM_Egon_BergerWaldenegg
		AUS_AM_Julius_Raab
		AUS_MoS_Eduard_BaarBaarenfels
		AUS_HoI_Wilhelm_Gebauer
	# Military Staff
		AUS_CoStaff_Alfred_von_Jansa
		AUS_CoArmy_Wilhelm_Zehner
		AUS_CoNavy_Paul_von_Hegedus
		AUS_CoAir_Alexander_Lohr
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Engelbert Dollfuss"
		desc = ""
		picture = "P_F_Engelbert_Dollfuss.tga"
		expire = "1965.1.1"
		ideology = clerical_fascism
		traits = {}
	}
	# Paternal Autocracy
	create_country_leader = {
		name = "Otto I"
		desc = ""
		picture = "P_A_Otto_I.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	#  Democracy
	create_country_leader = {
		name = "Karl Renner"
		desc = ""
		picture = "P_D_Karl_Renner.tga"
		expire = "1965.1.1"
		ideology = social_democracy
		traits = {}
	}
	# Left-Wing Radicalism
	create_country_leader = {
		name = "Friedrich Adler"
		desc = ""
		picture = "P_S_Friedrich_Adler.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	create_country_leader = {
		name = "Robert Danneberg"
		desc = ""
		picture = "P_S_Robert_Danneberg.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Johann Koplenig"
		desc = ""
		picture = "P_C_Johann_Koplenig.tga"
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {}
	}
	#######################
	# Generals
	#######################
	create_corps_commander = {
		name = "Karl Eglseer"
		portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
		traits = { trait_mountaineer trickster }
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 2
	}	
}
#########################################################################
# Austria - 1936
#########################################################################
1936.1.1 = {
	oob = "AUS_1936"
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		Small_Arms_1936 = 1
		tech_recon = 1
		tech_engineers = 1
		gw_artillery = 1
		interwar_antiair = 1
		tech_support = 1		
		tech_mountaineers = 1
		Fighter_1933 = 1
		Strategic_Bomber_1933 = 1
	# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = fascist
		last_election = "1930.11.9"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 50
		authoritarian = 0
		democratic = 38
		socialist = 12
		communist = 0
	}	
	add_ideas = {
	# Cabinet
		AUS_HoG_Kurt_Schuschnigg
		AUS_FM_Egon_BergerWaldenegg
		AUS_AM_Julius_Raab
		AUS_MoS_Eduard_BaarBaarenfels
		AUS_HoI_Wilhelm_Gebauer
	# Military Staff
		AUS_CoStaff_Alfred_von_Jansa
		AUS_CoArmy_Wilhelm_Zehner
		AUS_CoNavy_Paul_von_Hegedus
		AUS_CoAir_Alexander_Lohr
	}	
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Kurt Schuschnigg"
		desc = ""
		picture = "P_F_Kurt_Schuschnigg.tga"
		expire = "1965.1.1"
		ideology = clerical_fascism
		traits = {}
	}
	# Paternal Autocracy
	create_country_leader = {
		name = "Otto I"
		desc = ""
		picture = "P_A_Otto_I.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	#  Democracy
	create_country_leader = {
		name = "Karl Renner"
		desc = ""
		picture = "P_D_Karl_Renner.tga"
		expire = "1965.1.1"
		ideology = social_democracy
		traits = {}
	}
	# Left-Wing Radicalism
	create_country_leader = {
		name = "Friedrich Adler"
		desc = ""
		picture = "P_S_Friedrich_Adler.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	create_country_leader = {
		name = "Robert Danneberg"
		desc = ""
		picture = "P_S_Robert_Danneberg.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Johann Koplenig"
		desc = ""
		picture = "P_C_Johann_Koplenig.tga"
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {}
	}
}