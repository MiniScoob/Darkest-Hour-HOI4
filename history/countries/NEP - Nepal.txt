﻿capital = 323

oob = "NEP_1936"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	tech_mountaineers = 1
}

set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascist = 0
	authoritarian = 60
	democratic = 30
	socialist = 10
	communist = 0
}
create_country_leader = {
	name = "Tribhuvana"
	desc = "POLITICS_JUDDHA_RANA_DESC"
	picture = "P_A_Tribhuvan.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {
		#
	}
}