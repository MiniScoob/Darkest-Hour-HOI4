﻿#########################################################################
# Empire of Japan - 1933
#########################################################################
1933.1.1 = {
	capital = 282
	set_stability = 0.55
	set_war_support = 0.7
	oob = "JAP_1933"
	set_research_slots = 5
	set_convoys = 400

	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		truck_1936 = 1
		tech_support = 1
		tech_recon = 1
		tech_engineers = 1
		marines  = 1
		gw_artillery = 1
		interwar_antiair = 1

		cv_Fighter_1933 = 1
		Fighter_1933 = 1
		Tactical_Bomber_1933 = 1
		Naval_Bomber_1936 = 1
		CAS_1936 = 1
		Strategic_Bomber_1933 = 1
		# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
		Tank_1926 = 1
		Light_Tank_1934 = 1
		# Air Tech
		cv_CAS_1933 = 1
		#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1

		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		CL_1922 = 1
		CL_1933 = 1

		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1
		CA_1922 = 1
		CA_1933 = 1

		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
		SS_1933 = 1

		HSS_1933 = 1

		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1
		BC_1933 = 1

		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1
		BB_1906 = 1
		BB_1912 = 1
		BB_1916 = 1
		BB_1922 = 1
		BB_1933 = 1

		CV_1912 = 1
		CV_1916 = 1

		CV_1922 = 1
		CV_1930 = 1
		CV_1933 = 1

		transport = 1

		Trench_Warfare = 1
		base_strike = 1
		synth_oil_experiments = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1933.1.1"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 2
		authoritarian = 30
		democratic = 48
		socialist = 16
		communist = 4
	}
	add_ideas = {
	# Laws & Policies
		limited_exports
		limited_conscription
		partial_economic_mobilisation
	# Cabinet
		JAP_HoG_Saito_Makoto
		JAP_FM_Uchida_Kosai
		JAP_MoS_Yamamoto_Tatsuo
		JAP_HoI_Okamura_Yasuji
	# Military Staff
		JAP_CoStaff_Kanin_Kotohito
		JAP_CoArmy_Kanin_Kotohito
		JAP_CoNavy_Osumi_Mineo
		JAP_CoAir_Yamamoto_Isoroku
	}


	#######################
	# Diplomacy
	#######################
	create_faction = "Co-Prosperity Sphere"
	add_to_faction = JAP
	add_to_faction = MAN
	add_to_faction = MEN
	set_autonomy = {
		target = MEN
		autonomous_state = autonomy_puppet
		freedom_level = 0.35
	}
	set_autonomy = {
		target = MAN
		autonomous_state = autonomy_puppet
		freedom_level = 0.35
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Seigo Nakano"
		desc = ""
		picture = "Japan_F_Seigo_Nakano.tga"
		expire = "1965.1.1"
		ideology = fascism
		traits = {}
	}
	# Autocracy
	create_country_leader = {
		name = "Saito Makoto"
		desc = "Saito_desc"
		picture = "P_A_Saito_Makoto.tga"
		expire = "1938.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Hitoshi Ashida"
		desc = ""
		picture = "P_L_Hitoshi_Ashida.tga"
		expire = "1965.1.1"
		ideology = social_liberalism
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Mosaburō Suzuki"
		desc = ""
		picture = "P_S_Mosaburou_Suzuki.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Kyuichi Tokuda"
		desc = ""
		picture = "P_C_Kyuichi_Tokuda.tga"
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {}
	}

}

#########################################################################
# Empire of Japan - 1936
#########################################################################
1936.1.1 = {
	capital = 282
	set_stability = 0.65
	set_war_support = 0.8
	oob = "JAP_1936"
	set_research_slots = 5
	set_convoys = 400
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		Small_Arms_1936 = 1
		tech_support = 1
		tech_recon = 1
		tech_engineers = 1
		truck_1936 = 1
		marines  = 1
		gw_artillery = 1

		cv_Fighter_1933 = 1
		cv_CAS_1936 = 1
		Torpedo_bomber_1936 = 1
		Fighter_1933 = 1
		Naval_Bomber_1936 = 1
		Tactical_Bomber_1933 = 1
		CAS_1936 = 1
		Strategic_Bomber_1933 = 1
		# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
		Tank_1926 = 1
		Light_Tank_1934 = 1
		# Air Tech
		cv_CAS_1933 = 1
		#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1

		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		CL_1922 = 1
		CL_1933 = 1
		CL_1936 = 1

		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1
		CA_1922 = 1
		CA_1933 = 1
		CA_1936 = 1

		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
		SS_1933 = 1

		HSS_1933 = 1

		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1
		BC_1933 = 1

		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1
		BB_1906 = 1
		BB_1912 = 1
		BB_1916 = 1
		BB_1922 = 1
		BB_1933 = 1

		CV_1912 = 1
		CV_1916 = 1

		CV_1922 = 1
		CV_1930 = 1
		CV_1933 = 1
		CV_1936 = 1

		transport = 1

		Trench_Warfare = 1
		base_strike = 1
		synth_oil_experiments = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 10
		authoritarian = 46
		democratic = 42
		socialist = 2
		communist = 0
	}
	add_ideas = {
	# National Spirits
		state_shintoism
		JAP_Emperor_Hirohito
		JAP_zaibatsus
		JAP_interservice_rivalry_balanced
		JAP_Military_Outweighs_The_Government_bad
		JAP_Tokko
	# Laws & Policies
		limited_exports
		limited_conscription
		partial_economic_mobilisation
	# Cabinet
		JAP_HoG_Okada_Keisuke
		JAP_FM_Hirota_Koki
		JAP_MoS_Goto_Fumio
		JAP_HoI_Okamura_Yasuji
	# Military Staff
		JAP_CoStaff_Kanin_Kotohito
		JAP_CoArmy_Kanin_Kotohito
		JAP_CoNavy_Osumi_Mineo
		JAP_CoAir_Yamamoto_Isoroku
	}

	# Focus Tree
	unlock_national_focus = JAP_Sign_The_Tanggu_Truce
	unlock_national_focus = JAP_The_Manchukuo_Question
	unlock_national_focus = JAP_Okada_Cabinet

	#######################
	# Diplomacy
	#######################
	create_faction = "Dai Toa Kyoeiken"
	add_to_faction = JAP
	add_to_faction = MAN
	add_to_faction = RNG
	add_to_faction = EHB
	create_import = {
		resource = oil
		factories = 2
		exporter = USA
	}

	set_autonomy = {
		target = MAN
		autonomous_state = autonomy_puppet
		freedom_level = 0.35
	}
	set_autonomy = {
		target = RNG
		autonomous_state = autonomy_puppet
		freedom_level = 0.35
	}
	set_autonomy = {
		target = EHB
		autonomous_state = autonomy_puppet
		freedom_level = 0.35
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Seigo Nakano"
		desc = ""
		picture = "Japan_F_Seigo_Nakano.tga"
		expire = "1965.1.1"
		ideology = fascism
		traits = {}
	}
	# Autocracy
	create_country_leader = {
		name = "Okada Keisuke"
		desc = "Okada_desc"
		picture = "P_A_Okada_Keisuke.tga"
		expire = "1938.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Hitoshi Ashida"
		desc = ""
		picture = "P_L_Hitoshi_Ashida.tga"
		expire = "1965.1.1"
		ideology = social_liberalism
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Mosaburō Suzuki"
		desc = ""
		picture = "P_S_Mosaburou_Suzuki.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Kyuichi Tokuda"
		desc = ""
		picture = "P_C_Kyuichi_Tokuda.tga"
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {}
	}

}

	#######################
	# Generals
	#######################
	create_corps_commander = {
		name = "Tomoyuki Yamashita"
		picture = "M_Tomoyuki_Yamashita.tga"
		traits = {
			trickster trait_engineer brilliant_strategist politically_connected
		}
		skill = 5
		attack_skill = 4
		defense_skill = 5
		planning_skill = 3
		logistics_skill = 4
	}

	create_field_marshal = {
		name = "Hisaichi Terauchi"
		picture = "M_Hisiachi_Terauchi.tga"
		traits = {
			offensive_doctrine
			JAP_samurai_lineage   # hakushaku
			politically_connected
		}
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 2
	}

	create_field_marshal = {
		name = "Shunroku Hata"
		picture = "M_Shunroku_Hata.tga"
		traits = { offensive_doctrine JAP_samurai_lineage }
		skill = 4
		attack_skill = 3
		defense_skill = 4
		planning_skill = 2
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Akira Muto"
		picture = "M_Akira_Muto.tga"
		traits = { career_officer  }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kanji Ishiwara"
		picture = "M_Kanji_Ishiwara.tga"
		traits = { JAP_samurai_lineage }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Shizuichi Tanaka"
		picture = "M_Shizuichi_Tanaka.tga"
		traits = { trickster career_officer infantry_officer }
		skill = 4
		attack_skill = 4
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Yasuji Okamura"
		picture = "M_Yasuji_Okamura.tga"
		traits = { trait_reckless }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Toshizo Nishio" #Toshizo
		picture = "M_Toshizo_Nishio.tga"
		traits = { brilliant_strategist infantry_officer }
		skill = 3
		attack_skill = 3
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Rikichi Ando" #Ando
		picture = "M_Ando_Rikichi.tga"
		traits = { career_officer }
		skill = 3
		attack_skill = 1
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Naruhiko Higashikuni"
		picture = "M_Naruhiko_Higashikuni.tga"
		traits = { JAP_samurai_lineage politically_connected }
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Seishiro Itagaki"
		picture = "M_Seishiro_Itagaki.tga"
		traits = { JAP_samurai_lineage inflexible_strategist trait_reckless }
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Kenji Doihara"
		picture = "M_Kenji_Doihara.tga"
		traits = { career_officer substance_abuser }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kenkichi Ueda"
		picture = "M_Kenkichi_Ueda.tga"
		traits = { old_guard cavalry_officer trait_reckless }
		skill = 4
		attack_skill = 3
		defense_skill = 2
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "M_Shigeru Honjo"
		picture = "Shigeru_Honjo.tga"
		traits = { old_guard war_hero }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Yoshijiro Umezu"
		picture = "M_Yoshijiro_Umezu.tga"
		traits = { trait_cautious politically_connected }
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 1
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Otozo Yamada"
		picture = "M_Otozo_Yamada.tga"
		traits = { cavalry_officer career_officer }
		skill = 3
		attack_skill = 3
		defense_skill = 1
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Hatazo Adachi"
		picture = "M_Hatazo_Adachi.tga"
		traits = { trickster JAP_samurai_lineage trait_reckless infantry_officer }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Iwane Matsui"
		picture = "M_Iwane_Matsui.tga"
		traits = { old_guard trait_reckless }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Sadao Araki"
		picture = "Japan_F_Sadao_Araki.tga"
		traits = { JAP_samurai_lineage }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Keisuke Fujie"
		picture = "M_Keisuke_Fujie.tga"
		traits = { fortress_buster career_officer politically_connected }
		skill = 3
		attack_skill = 1
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Kiichiro Higuchi"
		picture = "M_Kiichiro_Higuchi.tga"
		traits = { career_officer trait_cautious }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Masaharu Homma"
		picture = "M_Masaharu_Homma.tga"
		traits = { media_personality trait_cautious }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Harukichi Hyakutake"
		picture = "M_Harukichi_Hyakutake.tga"
		traits = { infantry_officer  }
		skill = 3
		attack_skill = 2
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Jo Iimura"
		picture = "Jo_Iimura.tga"
		traits = { career_officer }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Hitoshi Imamura"
		picture = "Hitoshi_Imamura.tga"
		traits = {  commando career_officer infantry_officer }
		skill = 3
		attack_skill = 3
		defense_skill = 1
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Masatane Kanda"
		picture = "Masatane_Kanda.tga"
		traits = { trait_cautious }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Kuniaki Koiso"
		picture = "P_A_Kuniaki_Koiso.tga"
		traits = { JAP_samurai_lineage old_guard politically_connected }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 1
	}
	#######################
	# Admirals
	#######################
	create_navy_leader = {
		name = "Isoroku Yamamoto"
			picture = "M_Isoroku_Yamamoto.tga"
		traits = { superior_tactician spotter }
		skill = 5
	}

	create_navy_leader = {
		name = "Mineichi Koga"
		picture = "M_Mineichi_Koga.tga"
		traits = { superior_tactician spotter }
		skill = 4
	}

	create_navy_leader = {
		name = "Kiyoshi Hasegawa"
		picture = "M_Hasegawa_Kiyoshi.tga"
		traits = {  }
		skill = 2
	}

	# Keen on planes
	create_navy_leader = {
		name = "Shigeyoshi Inoue"
		picture = "M_Shigeyoshi_Inoue.tga"
		traits = { air_controller }
		skill = 1
	}

	create_navy_leader = {
		name = "Nobutake Kondo"
		picture = "M_Nobutake_Kondo.tga"
		traits = { fly_swatter }
		skill = 3
	}

	create_navy_leader = {
		name = "Takeo Takagi"
			picture = "Takeo_Takagi.tga"
		traits = {  }
		skill = 2
	}

	create_navy_leader = {
		name = "Soemu Toyoda"
			picture = "Soemu_Toyoda.tga"
		traits = { old_guard_navy }
		skill = 1
	}

	create_navy_leader = {
		name = "Jisaburo Ozawa"
			picture = "Jisaburo_Ozawa.tga"
		traits = { blockade_runner superior_tactician }
		skill = 5
		}

	create_navy_leader = {
		name = "Zengo Yoshida"
			picture = "Zengo_Yoshida.tga"
		traits = {  }
		skill = 2
	}

	create_navy_leader = {
		name = "Hiroaki Abe"
			picture = "Hiroaki_Abe.tga"
		traits = {  }
		skill = 1
	}

	create_navy_leader = {
		name = "Tadashige Daigo"
			picture = "Tadashige_Daigo.tga"
		traits = { seawolf }
		skill = 1
	}

	create_navy_leader = {
		name = "Gengo Hyakutake"
			picture = "Gengo_Hyakutake.tga"
		traits = {  }
		skill = 3
	}
#########################################################################
# Empire of Japan - 1944
#########################################################################
1944.6.20 = {
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = fascist
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 88
		authoritarian = 10
		democratic = 0
		socialist = 2
		communist = 0
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Hideki Tojo"
		desc = ""
		picture = "Japan_F_Hideki_Tojo.tga"
		expire = "1965.1.1"
		ideology = fascism
		traits = {}
	}
	# Autocracy
	create_country_leader = {
		name = "Naruhiko Higashikuni"
		desc = "Naruhiko_desc"
		picture = "M_Naruhiko_Higashikuni.tga"
		expire = "1938.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Hitoshi Ashida"
		desc = ""
		picture = "P_L_Hitoshi_Ashida.tga"
		expire = "1965.1.1"
		ideology = social_liberalism
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Mosaburō Suzuki"
		desc = ""
		picture = "P_S_Mosaburou_Suzuki.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Kyuichi Tokuda"
		desc = ""
		picture = "P_C_Kyuichi_Tokuda.tga"
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {}
	}
}
if = {
	limit = {
		has_start_date > 1944.06.19
		has_start_date < 1944.06.21
	}
	declare_war_on = {
		target = CHI
		type = annex_everything
	}
	declare_war_on = {
		target = SHD
		type = annex_everything
	}
	declare_war_on = {
		target = HCP
		type = annex_everything
	}
}
#########################################################################
# Occupied Japan - 1946
#########################################################################
1946.1.1 = {
	#######################
	# Politics
	#######################
	set_cosmetic_tag = JAP_OCCUPIED

	set_politics = {
		ruling_party = authoritarian
		last_election = "1933.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 100
		democratic = 0
		socialist = 0
		communist = 0
	}
	#######################
	# Leaders
	#######################
	# Autocracy
	create_country_leader = {
		name = "Douglas MacArthur"
		desc = ""
		picture = "P_USA_A_Douglas_MacArthur.tga"
		expire = "1952.1.1"
		ideology = military_dictatorship
		traits = {}
	}
	#######################
	# Diplomacy
	#######################
	dismantle_faction = yes
	set_autonomy = {
		target = MEN
		autonomous_state = autonomy_free
	}
	set_autonomy = {
		target = MAN
		autonomous_state = autonomy_free
	}
	set_autonomy = {
		target = RNG
		autonomous_state = autonomy_free
	}
	set_autonomy = {
		target = EHB
		autonomous_state = autonomy_free
	}
	JAP ={
		white_peace = CHI
		white_peace = SHD
		white_peace = HCP
	}
}
