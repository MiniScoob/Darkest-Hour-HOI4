﻿################################
# Lituania - 1933
################################
1933.1.1 = {
	oob = "LIT_1933"
}
################################
# Lituania - 1936
################################
1936.1.1 = {
	oob = "LIT_1936"
	set_technology = {
		Small_Arms_1936 = 1
	}
}
capital = 11
set_research_slots = 3
add_ideas = {
	limited_conscription
}

set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 0
	Fighter_1933 = 1
}

set_convoys = 10

set_politics = {
	ruling_party = authoritarian
	last_election = "1926.5.8"
	election_frequency = 120
	elections_allowed = no
}
set_popularities = {
	fascist = 2
	authoritarian = 60
	democratic = 38
	socialist = 0
	communist = 0
}
####################################################
# Lithunian Leaders
####################################################
#  Autocracy
create_country_leader = {
	name = "Antanas Smetona"
	desc = "POLITICS_ANTANAS_SMETONA_DESC"
	picture = "P_A_Antanas_Smetona.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
