﻿#########################################################################
# Guangxi
#########################################################################
capital = 594
set_stability = 0.6
set_war_support = 0.4
oob = "GXC_1936"
set_convoys = 5
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
	Mass_Assault = 1
#Naval Stuff
	DD_1885 = 1
	
	CL_1885 = 1
	CL_1900 = 1
	CL_1912 = 1
	CL_1922 = 1
	
	BB_1885 = 1
	BB_1895 = 1
	
	CA_1885 = 1
	CA_1895 = 1
#
}
#######################
# Politics
#######################
set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascist = 0
	authoritarian = 60
	democratic = 40
	socialist = 0
	communist = 0
}
add_ideas = {
}

#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Li Zongren"
	desc = "POLITICS_LI_ZONGREN_DESC"
	picture = "P_A_Li_Zongren.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Bai Chongxi"
	picture = "M_Pai_Ch'ung-hsi.tga"
	traits = { inspirational_leader }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}
create_field_marshal = {
	name = "Li Zongren"
	picture = "P_A_Li_Zongren.tga"
	traits = { defensive_doctrine trait_cautious politically_connected }
	skill = 4
	attack_skill = 2
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}