﻿#########################################################################
# Kingdom of Egypt - 1933
#########################################################################
1933.1.1 = {
	capital = 446

	# Starting tech
	set_technology = {
		Small_Arms_1918 = 1
	}

	set_convoys = 20

	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 60
		democratic = 40
		socialist = 0
		communist = 0
	}
	###############################
	# Egyptian Leaders 
	###############################
	# Fascism
	create_country_leader = {
		name = "Ahmed Husayn"
		desc = ""
		picture = ".tga"
		expire = "1965.1.1"
		ideology = fascism
		traits = {}
	}
	# Paternal Autocracy
	create_country_leader = {
		name = "Farouk I"
		desc = ""
		picture = "P_A_Farouk_I.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	create_country_leader = {
		name = "Fuad I"
		desc = ""
		picture = "P_A_Fuad_I.tga"
		expire = "1936.4.31"
		ideology = authoritarian_democracy
		traits = {}
	}
	# Conservatism
	create_country_leader = {
		name = "Ali Mahir Pasha"
		desc = ""
		picture = ".tga"
		expire = "1938.1.1"
		ideology = social_conservatism
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Gamal Abdel Nasser"
		desc = ""
		picture = "P_S_Gamel_Abdul_Nasser.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
	# Communism
	create_country_leader = {
		name = "Abdel Hakim Amer"
		desc = ""
		picture = ".tga"
		expire = "1965.1.1"
		ideology = trotskyism
		traits = {}
	}
}
#########################################################################
# Arab Republic of Egypt - 1953
#########################################################################
1953.1.1 = {
	set_cosmetic_tag = EGY_ARABREP
	###############################
	# Leaders 
	###############################
	# Paternal Autocracy
	create_country_leader = {
		name = "Mohamed Naguib"
		desc = ""
		picture = "P_A_Mohamed_Naguib.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = {}
	}
}
#########################################################################
# Arab Republic of Egypt - 1960
#########################################################################
1960.1.1 = {
	###############################
	# Politics 
	###############################
	set_cosmetic_tag = EGY_UAR
	set_politics = {
		ruling_party = socialist
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 5
		authoritarian = 15
		democratic = 0
		socialist = 80
		communist = 0
	}

	###############################
	# Leaders 
	###############################
	# Socialism
	create_country_leader = {
		name = "Gamal Abdel Nasser"
		desc = ""
		picture = "P_S_Gamel_Abdul_Nasser.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = {}
	}
}