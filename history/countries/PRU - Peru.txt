﻿#########################################################################
# Peru - 1933
#########################################################################
1933.1.1 = {
	capital = 303
	oob = "PRU_1933"
	set_convoys = 5
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		gw_artillery = 1
		Fighter_1933 = 1
		cv_Fighter_1933 = 1
		Tactical_Bomber_1933 = 1
		CAS_1936 = 1
	#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		
		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
	#
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1931.10.11"
		election_frequency = 96
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 51
		democratic = 44
		socialist = 5
		communist = 0
	}	
	#######################
	# Diplomacy
	#######################
	set_country_flag = monroe_doctrine
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Luis A. Flores"
		desc = ""
		picture = "P_F_Luis_A_Flores.tga"
		expire = "1965.1.1"
		ideology = fascism
		traits = {}
	}
	# Autocracy
	create_country_leader = {
		name = "Oscar Benavides Larrea"
		desc = ""
		picture = "P_A_Oscar_Benavides.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Luis Antonio Eguiguren"
		desc = ""
		picture = "P_D_Luis_Antonio_Eguiguren.tga"
		expire = "1965.1.1"
		ideology = social_democracy
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Luciano Castillo Colonn"
		desc = ""
		picture = "P_S_Luciano_Castillo_Colonn.tga"
		expire = "1965.1.1"
		ideology = democratic_socialism
		traits = {}
	}
	# Communism
	create_country_leader = {
		name = "Eudocio Ravines"
		desc = ""
		picture = "P_C_Eudocio_Ravines.tga"
		expire = "1965.1.1"
		ideology = leninism
		traits = {}
	}
} 
#########################################################################
# Peru - 1936
#########################################################################
1936.1.1 = {
	oob = "PRU_1936"
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		gw_artillery = 1
		Fighter_1933 = 1
		tech_support = 1
		tech_engineers = 1
		cv_Fighter_1933 = 1
		Tactical_Bomber_1933 = 1
		CAS_1936 = 1
	#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		
		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
	#
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1931.10.11"
		election_frequency = 96
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 51
		democratic = 44
		socialist = 5
		communist = 0
	}		
	#######################
	# Diplomacy
	#######################
	set_country_flag = monroe_doctrine
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Luis A. Flores"
		desc = ""
		picture = "P_F_Luis_A_Flores.tga"
		expire = "1965.1.1"
		ideology = fascism
		traits = {}
	}
	# Autocracy
	create_country_leader = {
		name = "Luis Miguel Sánchez Cerro"
		desc = ""
		picture = "P_A_Luis_Miguel_Sanchez_Cerro.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	# Democracy
	create_country_leader = {
		name = "Luis Antonio Eguiguren"
		desc = ""
		picture = "P_D_Luis_Antonio_Eguiguren.tga"
		expire = "1965.1.1"
		ideology = social_democracy
		traits = {}
	}
	# Socialism
	create_country_leader = {
		name = "Luciano Castillo Colonn"
		desc = ""
		picture = "P_S_Luciano_Castillo_Colonn.tga"
		expire = "1965.1.1"
		ideology = democratic_socialism
		traits = {}
	}
	# Communism
	create_country_leader = {
		name = "Eudocio Ravines"
		desc = ""
		picture = "P_C_Eudocio_Ravines.tga"
		expire = "1965.1.1"
		ideology = leninism
		traits = {}
	}
}