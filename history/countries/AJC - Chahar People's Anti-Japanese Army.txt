﻿#########################################################################
# Chahar Army
#########################################################################
capital = 608
set_stability = 0.6
set_war_support = 0.6
oob = "AJC_1933"
set_convoys = 5
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
	mass_assault = 1
#
}
#######################
# Politics
#######################
set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascist = 0
	authoritarian = 100
	democratic = 0
	socialist = 0
	communist = 0
}

#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Feng Yu-hsiang"
	desc = "Feng_Yu-hsiang_desc"
	picture = "P_A_Feng_Yu-hsiang.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
# Communism
create_country_leader = {
	name = "Yang Chingyu"
	desc = "Jingyu_desc"
	picture = "P_A_Yang_Chingyu.tga"
	expire = "1965.1.1"
	ideology = maoism
	traits = { cornered_fox }
}

