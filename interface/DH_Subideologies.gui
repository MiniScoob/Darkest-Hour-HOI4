guiTypes = {
	####################################
	#  Secondary Leader GUI (Politics)
	####################################
	containerWindowType = {
		name = "politics_secondary_leader_gui"
        position = { x = 16 y = 0 }
		size = { width = 200 height = 200 }

		instantTextboxType = {
			name = "leader_title"
			position = { x = 0 y = 52 }
			font = "Aspira_19"
			text = "[This.leader_title_one]"
			maxWidth = 155
			maxHeight = 20
			format = center
			vertical_alignment = center
		}
		
		instantTextboxType = {
			name = "secondary_leader_title"
			position = { x = 169 y = 52 }
			font = "Aspira_19"
			text = "[This.leader_title_two]"
			maxWidth = 155
			maxHeight = 20
			format = center
			vertical_alignment = center
		}
		
		instantTextboxType = {
			name = "politics_leader_name"
			position = { x = 169 y = 300 }
			font = "Aspira_19"
			text = "[This.leader_name_two]"
			maxWidth = 155
			maxHeight = 20
			format = center
			vertical_alignment = center
			pdx_tooltip = "SECOND_LEADER_NAME"
			pdx_tooltip_delayed = "SECOND_LEADER_DESC"
		}
		
		iconType = {
			name = "secondary_leader_portrait"
			position = { x = 167 y = 80 }
			spriteType = "GFX_leader_unknown"
			pdx_tooltip = "SECOND_LEADER_NAME"
			pdx_tooltip_delayed = "SECOND_LEADER_DESC"
		}
		
		iconType = {
			name = "secondary_leader_portrait_frame"
			spriteType = "GFX_pol_leader_frame"
			position = { x = 167 y = 80 }
			pdx_tooltip = "SECOND_LEADER_NAME"
			pdx_tooltip_delayed = "SECOND_LEADER_DESC"
		}
	}
	
	####################################
	#  Secondary Leader GUI (Diplo View)
	####################################
	containerWindowType = {
		name = "politics_secondary_leader_diplo_gui"
        position = { x = 0 y = 0 }
		size = { width = 200 height = 200 }
		
		iconType = {
			name = "leader_portrait_overlay"
			position = { x = 23 y = 65 }
			spriteType = "GFX_leader_portrait_transparent"
			scale = 0.74
			pdx_tooltip = "LEADER_NAME_DIPLO_VIEW"
		}
		
		iconType = {
			name = "secondary_leader_portrait"
			position = { x = 145 y = 68 }
			spriteType = "GFX_leader_unknown"
			scale = 0.73
			pdx_tooltip = "SECOND_LEADER_NAME_DIPLO_VIEW"
		}
	}

	####################################
	#  Subideology Icons (Politics)
	####################################
	containerWindowType = {
		name = "subideology_icons_window"
        position = { x = 355 y = 61 }
		size = { width = 90 height = 90 }

		iconType = {
			name = "Clerical_Fascism_icon"
			spriteType = "GFX_Clerical_Fascism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Esoteric_National_Socialism_icon"
			spriteType = "GFX_Esoteric_National_Socialism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Falangism_icon"
			spriteType = "GFX_Falangism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Fascism_icon"
			spriteType = "GFX_Fascism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Integralism_icon"
			spriteType = "GFX_Integralism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Legionarism_icon"
			spriteType = "GFX_Legionarism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Metaxism_icon"
			spriteType = "GFX_Metaxism_icon"
			position = { x = 0 y = -2 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "National_Socialism_icon"
			spriteType = "GFX_National_Socialism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Revisionist_Maximalism_icon"
			spriteType = "GFX_Revisionist_Maximalism_icon"
			position = { x = 0 y = 2 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Rexism_icon"
			spriteType = "GFX_Rexism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Salafism_icon"
			spriteType = "GFX_Salafism_icon"
			position = { x = 2 y = 2 }
			alwaystransparent = yes
			scale = 0.75
		}
		
		iconType = {
			name = "Strasserism_icon"
			spriteType = "GFX_Strasserism_icon"
			position = { x = 1 y = 1 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Authoritarian_Democracy_icon"
			spriteType = "GFX_Authoritarian_Democracy_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Military_Dictatorship_icon"
			spriteType = "GFX_Military_Dictatorship_icon"
			position = { x = 10 y = 6 }
			alwaystransparent = yes
			scale = 0.6
		}
		
		iconType = {
			name = "Monarchism_icon"
			spriteType = "GFX_Monarchism_icon"
			position = { x = 0 y = 0 }
			alwaystransparent = yes
			scale = 0.8
		}
		
		iconType = {
			name = "Theocracy_icon"
			spriteType = "GFX_Theocracy_icon"
			position = { x = 5 y = 10 }
			alwaystransparent = yes
			scale = 0.7
		}
	}
}